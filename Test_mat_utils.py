# Import:
from assoc_utils import assoc_mat_util
import numpy as np

# Define datadir, mouse_id and filename
datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_id = "CTBD7.1d"
filename = datadir+mouse_id+".mat"

# Running multi_mat_util functions to check if applicable to rscassoc

#loads the mat file
mat_data = assoc_mat_util.load_mat_file(filename)

# # changes format
# assoc_mat_util.get_hdf5group_keys(mat_data)
#
# # prints .mat keys (not values, only keys)
# assoc_mat_util.print_file_content(mat_data)
#
# # saves as pickle
# assoc_mat_util.save_zipped_pickle(filename, 'new')
#
# # formats date to match the .mat structure
# date = '20190202'
# print(assoc_mat_util.make_date_key(date))
#
# # formats area to match the .mat structure
# area = 1
# print(assoc_mat_util.make_area_key(area))
#
# # formats plane to match the .mat structure
# plane = 1
# print(assoc_mat_util.make_plane_key(plane))
#
# # gets mat structure - string
# assoc_mat_util.get_mat_str()
#
# # gets all the date keys from the .mat file
# print(assoc_mat_util.get_date_keys(mat_data))
#
# # gets areas corresponding to the date from the .mat file
# print(assoc_mat_util.get_area_keys(mat_data,'date_2019_01_30'))
#
# # gets plane corresponding to the date and area
# print(assoc_mat_util.get_plane_keys(mat_data,'date_2019_01_30', 'area2'))
#
# # gets planes corresponding to sess_bah, date and area
# print(assoc_mat_util.get_planes_sess_beh_keys(mat_data, 'date_2019_01_30', 'area1'))
#
# # gets raw florescence data
# print(assoc_mat_util.get_fluo_raw(mat_data,'date_2019_01_30', 'area1','plane1'))
#
# # corrected raw florescence data
# print(assoc_mat_util.get_fluo_corr(mat_data,'date_2019_01_30', 'area1','plane1'))
#
# # gets raw neuropil
# print(assoc_mat_util.get_neuropil_raw(mat_data,'date_2019_01_30', 'area1','plane1'))
#
# # gets trial by trial florescence data
# print(assoc_mat_util.get_fluo_trialbytrial(mat_data,'date_2019_01_30', 'area1','plane1'))
#
# # gets number of rois identified
# print(assoc_mat_util.get_nrois(mat_data,'date_2019_01_30', 'area1','plane1'))
#
# # gets number of corresponding trials and rois in the given date, area and plane
# print(assoc_mat_util.get_info(mat_data,'date_2019_01_30', 'area1','plane1'))
#
# # gets number of trials
# print(assoc_mat_util.get_ntrials(mat_data,'date_2019_01_30', 'area1','plane1'))
#
# # gets task name
# print(assoc_mat_util.get_task(mat_data,'date_2019_02_01', 'area1'))
#
# # gets mouse_if from the filepath/filename
# print(assoc_mat_util.get_mouseid_from_file(datadir))
#
# # gets mouseid corresponding to date and area
# print(assoc_mat_util.get_mouseid(mat_data,'date_2019_01_30', 'area1'))
#
# # checks mouseid
# print(assoc_mat_util.check_mouseid('CTBD7.1d',mat_data,'date_2019_01_30', 'area1'))
#
# # next mouse ID
# print(assoc_mat_util.get_next_mouse_n('CTBD7.1d'))
#
# # gets date
# print(assoc_mat_util.get_date(mat_data,'date_2019_01_30','area1'))
#
# # object stim_pos does not exist
# print(assoc_mat_util.get_stim_pos(mat_data,'date_2019_01_30', 'area1'))
#
# # trial outcome is specific to their task outcomes. Does not apply
# print(assoc_mat_util.get_trial_outc(mat_data,'date_2019_01_30', 'area1'))
#
# # gets run speed from velocity
# print(assoc_mat_util.get_run_speed(mat_data,'date_2019_01_30', 'area1'))
#
# # unable to find object 'lick'
# #print(assoc_mat_util.get_licks(mat_data,'date_2019_01_30', 'area1'))
#
# # gets frame rate
# print(assoc_mat_util.get_fps(mat_data,'date_2019_01_30', 'area1', 'plane1'))
#
# # gets roi position
# print(assoc_mat_util.get_roipos(mat_data,'date_2019_01_30', 'area1', 'plane1'))
#
# # no luck!
# print(assoc_mat_util.build_mouse_df(mat_data))
#
#
# ####################################################################################
# #Printing nested dictionaries to understand keys:
#
# # items in session_behaviour
# print([item for item in mat_data['imaging']['date_2019_01_31']['area1']['session_behaviour']])
#
# # items in plane1
# print([item for item in mat_data['imaging']['date_2019_01_31']['area1']['plane1']])
#
# # items in imaging
# print([item for item in mat_data['imaging']])
#
# save session_beh in a variable
beh_data = mat_data['imaging']['date_2019_01_30']['area2']['session_behaviour']
print([beh_data[item] for item in beh_data])
# print(beh_data['TTLs'][()])
# print(assoc_mat_util.get_mat_str(beh_data['ID'][()]))
# l = beh_data['lick1_event'][()].squeeze()
# # to check the max value of licks
# print([value for value in l if value>15000])


#
# # save flo data in a variable
# flo_data = mat_data['imaging']['date_2019_01_30']['area1']['plane1']
# print([flo_data[item] for item in flo_data])
#
# ###################################################################################
# # Testing new functions created
#
# # testing get_licks
get_licks = (assoc_mat_util.get_licks(mat_data,'date_2019_01_30', 'area2'))
print(len(get_licks))
# print(get_licks)

lick = assoc_mat_util.get_licks2_event(mat_data,'date_2019_01_30', 'area2')
print(len(lick))
# testing get_trial_start
# trial_start = (assoc_mat_util.get_trial_start(mat_data,'date_2019_02_03', 'area1'))
# print(len(trial_start))
#
# # #
# # # testing get_iti_start
iti_start = (assoc_mat_util.get_iti_start(mat_data,'date_2019_02_03', 'area1'))
# print(len(iti_start))
# print(list(range(len(iti_start))))
# # testing get_task
# print(assoc_mat_util.get_task(mat_data,'date_2019_01_30', 'area1'))
#
# # testing get lick_window
# print(assoc_mat_util.get_lick_window(mat_data,'date_2019_01_30', 'area1'))

# testing get TLLs
# print(assoc_mat_util.get_tlls(mat_data,'date_2019_01_30', 'area1'))

# n_trials = assoc_mat_util.get_ntrials(mat_data,'date_2019_01_30', 'area1', 'plane1')
# print(n_trials)
# trial_n = list(range(n_trials+1))
# print(trial_n)



# print(assoc_mat_util.get_fr_to_s(mat_data,'date_2019_01_30','area1', sec=4))
# print(assoc_mat_util.get_s_to_fr(mat_data,'date_2019_01_30','area1', sec=150))

