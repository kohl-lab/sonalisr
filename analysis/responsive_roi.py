import numpy as np
import session
import matplotlib.pyplot as plt
from scipy import stats
from assoc_utils import assoc_ml_util, assoc_gen_util

# Set paths to data directory and mouse dataframe
datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_df = "/Users/sonalisriranga/PycharmProjects/sonalisr/mouse_df.csv"
roi_file = "/Users/sonalisriranga/PycharmProjects/sonalisr/roi_tracking/CTBD11.1i/consitent_rois_plane1_ax234.csv"


# datadir = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - " \
#           "SONALISR/2p_processed_data/pipeline_output/imaging/"
# mouse_df = "/Users/anacarolinabotturabarros/PycharmProjects/sonalisr/mouse_df.csv"


# mouse_n = 1
sess1 = session.Session(datadir, area=1, mouse_n=2, date=20190131, mouse_df=mouse_df, roi_file=roi_file)
sess2 = session.Session(datadir, area=2, mouse_n=2, date=20190131, mouse_df=mouse_df, roi_file=roi_file)
sess3 = session.Session(datadir, area=2, mouse_n=1, date=20190203, mouse_df=mouse_df, roi_file=roi_file)
sess4 = session.Session(datadir, area=2, mouse_n=2, date=20190203, mouse_df=mouse_df, roi_file=roi_file)
sess5 = session.Session(datadir, area=1, mouse_n=1, date=20190204, mouse_df=mouse_df, roi_file=roi_file)


print('Analysing responsive rois from day {} where mouse {} received {} / {}'.format(sess1.date, sess1.mouseid, sess1.flavour_id,
                                                                                     sess1.flavour_code))
print('Analysing responsive rois from day {} where mouse {} received {} / {}'.format(sess2.date, sess2.mouseid, sess2.flavour_id,
                                                                                     sess2.flavour_code))
print('Analysing responsive rois from day {} where mouse {} received {} / {}'.format(sess3.date, sess3.mouseid, sess3.flavour_id,
                                                                                     sess3.flavour_code))
print('Analysing responsive rois from day {} where mouse {} received {} / {}'.format(sess4.date, sess4.mouseid, sess4.flavour_id,
                                                                                     sess4.flavour_code))
print('Analysing responsive rois from day {} where mouse {} received {} / {}'.format(sess5.date, sess5.mouseid, sess5.flavour_id,
                                                                                     sess5.flavour_code))


xran_full1, Tavg_less_act1 , Tavg_more_act1, Tavg_removed_data1  = assoc_ml_util.get_Tavg_more_less(sess1)
xran_full2, Tavg_less_act2 , Tavg_more_act2, Tavg_removed_data2  = assoc_ml_util.get_Tavg_more_less(sess2)
xran_full3, Tavg_less_act3 , Tavg_more_act3, Tavg_removed_data3  = assoc_ml_util.get_Tavg_more_less(sess3)
xran_full4, Tavg_less_act4 , Tavg_more_act4, Tavg_removed_data4  = assoc_ml_util.get_Tavg_more_less(sess4)
xran_full5, Tavg_less_act5 , Tavg_more_act5, Tavg_removed_data5  = assoc_ml_util.get_Tavg_more_less(sess5)
#############################################
# Concatenate the sessions together
# more_active = np.concatenate((Tavg_more_act1,Tavg_more_act2,Tavg_more_act3,Tavg_more_act4,Tavg_more_act5), axis=0)
# less_active = np.concatenate((Tavg_less_act1,Tavg_less_act2,Tavg_less_act3,Tavg_less_act4,Tavg_less_act5), axis=0)
# unresponsive_data = np.concatenate((Tavg_removed_data1,Tavg_removed_data2,Tavg_removed_data3,Tavg_removed_data4,Tavg_removed_data5), axis=0)

#############################################
# plt.plot(xran_full1, np.mean(Tavg_more_act1, axis=0), c='olivedrab')
plt.plot(xran_full1, np.mean(Tavg_less_act1, axis=0), c='royalblue')
plt.plot(xran_full1, np.mean(Tavg_removed_data1, axis=0), c='dimgrey')
# plt.fill_between(xran_full1, Tavg_more_act1.mean(axis=0) - stats.sem(Tavg_more_act1, axis=0), Tavg_more_act1.mean(axis=0) + stats.sem(Tavg_more_act1, axis=0), color='olivedrab', alpha=0.2,)
plt.fill_between(xran_full1, Tavg_less_act1.mean(axis=0) - stats.sem(Tavg_less_act1, axis=0), Tavg_less_act1.mean(axis=0) + stats.sem(Tavg_less_act1, axis=0), color='royalblue', alpha=0.2,)
plt.fill_between(xran_full1, Tavg_removed_data1.mean(axis=0) - stats.sem(Tavg_removed_data1, axis=0), Tavg_removed_data1.mean(axis=0) + stats.sem(Tavg_removed_data1, axis=0), color='dimgrey', alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--')
# plt.axvspan(0,0.5,facecolor='gainsboro', alpha=0.5)
plt.xlabel('Time(s)', fontsize=14)
plt.ylabel('Normalised Δf/f', fontsize=14)
plt.ylim(-0.03,0.03)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.legend(["Suppressed responsive (N=7)","Unresponsive (N=148)", "Stimulus onset"], loc='upper left' , fontsize=14)
plt.title("Means of responsive and unresponsive ROIs at stimulus onset on day 1 where mouse CTBD7.1d received Water / W")



plt.plot(xran_full2, np.mean(Tavg_more_act2, axis=0), c='olivedrab')
plt.plot(xran_full2, np.mean(Tavg_less_act2, axis=0), c='royalblue')
plt.plot(xran_full2, np.mean(Tavg_removed_data2, axis=0), c='dimgrey')
plt.fill_between(xran_full2, Tavg_more_act2.mean(axis=0) - stats.sem(Tavg_more_act2, axis=0), Tavg_more_act2.mean(axis=0) + stats.sem(Tavg_more_act2, axis=0), color='olivedrab', alpha=0.2,)
plt.fill_between(xran_full2, Tavg_less_act2.mean(axis=0) - stats.sem(Tavg_less_act2, axis=0), Tavg_less_act2.mean(axis=0) + stats.sem(Tavg_less_act2, axis=0), color='royalblue', alpha=0.2,)
plt.fill_between(xran_full2, Tavg_removed_data2.mean(axis=0) - stats.sem(Tavg_removed_data2, axis=0), Tavg_removed_data2.mean(axis=0) + stats.sem(Tavg_removed_data2, axis=0), color='dimgrey', alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--')
# plt.axvspan(0,0.5,facecolor='gainsboro', alpha=0.5)
plt.xlabel('Time(s)', fontsize=14)
plt.ylim(-0.03,0.03)
plt.ylabel('Normalised Δf/f', fontsize=14)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.legend(["Excited responsive (N=4)","Suppressed responsive (N=14)","Unresponsive (N=108)", "Stimulus onset"], loc='upper left', fontsize=14)
plt.title("Means of responsive and unresponsive ROIs at stimulus onset on day 3 where mouse CTBD11.1i received Saline / AX")

#
plt.plot(xran_full3, np.mean(Tavg_more_act3, axis=0), c='olivedrab')
plt.plot(xran_full3, np.mean(Tavg_less_act3, axis=0), c='royalblue')
plt.plot(xran_full3, np.mean(Tavg_removed_data3, axis=0), c='dimgrey')
plt.fill_between(xran_full3, Tavg_more_act3.mean(axis=0) - stats.sem(Tavg_more_act3, axis=0), Tavg_more_act3.mean(axis=0) + stats.sem(Tavg_more_act3, axis=0), color='olivedrab', alpha=0.2)
plt.fill_between(xran_full3, Tavg_less_act3.mean(axis=0) - stats.sem(Tavg_less_act3, axis=0), Tavg_less_act3.mean(axis=0) + stats.sem(Tavg_less_act3, axis=0), color='royalblue', alpha=0.2,)
plt.fill_between(xran_full3, Tavg_removed_data3.mean(axis=0) - stats.sem(Tavg_removed_data3, axis=0), Tavg_removed_data3.mean(axis=0) + stats.sem(Tavg_removed_data3, axis=0), color='dimgrey', alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--')
# plt.axvspan(0,0.5,facecolor='gainsboro', alpha=0.5)
plt.xlabel('Time(s)', fontsize=14)
plt.ylim(-0.03,0.03)
plt.ylabel('Normalised Δf/f', fontsize=1)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.legend(["Excited responsive (N=8)","Suppressed responsive (N=46)","Unresponsive (N=164)", "Stimulus onset"], loc='upper left', fontsize=14)
plt.title("Means of responsive and unresponsive ROIs at stimulus onset on day 5 where mouse CTBD11.1i received Saline / AX")

#
plt.plot(xran_full4, np.mean(Tavg_more_act4, axis=0), c='olivedrab')
plt.plot(xran_full4, np.mean(Tavg_less_act4, axis=0), c='royalblue')
plt.plot(xran_full4, np.mean(Tavg_removed_data4, axis=0), c='dimgrey')
plt.fill_between(xran_full4, Tavg_more_act4.mean(axis=0) - stats.sem(Tavg_more_act4, axis=0), Tavg_more_act4.mean(axis=0) + stats.sem(Tavg_more_act4, axis=0), color='olivedrab', alpha=0.2,)
plt.fill_between(xran_full4, Tavg_less_act4.mean(axis=0) - stats.sem(Tavg_less_act4, axis=0), Tavg_less_act4.mean(axis=0) + stats.sem(Tavg_less_act4, axis=0), color='royalblue', alpha=0.2,)
plt.fill_between(xran_full4, Tavg_removed_data4.mean(axis=0) - stats.sem(Tavg_removed_data4, axis=0), Tavg_removed_data4.mean(axis=0) + stats.sem(Tavg_removed_data4, axis=0), color='dimgrey', alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--')
# plt.axvspan(0,0.5,facecolor='gainsboro', alpha=0.5)
plt.xlabel('Time(s)', fontsize=14)
plt.ylabel('Normalised Δf/f', fontsize=14)
plt.ylim(-0.03,0.03)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.legend(["Excited responsive (N=23)","Suppressed responsive (N=42)","Unresponsive (N=175)", "Stimulus onset"], loc='upper left', fontsize=14)
plt.title("Means of responsive and unresponsive ROIs at stimulus onset on day 4 where mouse CTBD11.1i received Quinine / Y")


plt.plot(xran_full5, np.mean(Tavg_more_act5, axis=0), c='olivedrab')
plt.plot(xran_full5, np.mean(Tavg_less_act5, axis=0), c='royalblue')
plt.plot(xran_full5, np.mean(Tavg_removed_data5, axis=0), c='dimgrey')
plt.fill_between(xran_full5, Tavg_more_act5.mean(axis=0) - stats.sem(Tavg_more_act5, axis=0), Tavg_more_act5.mean(axis=0) + stats.sem(Tavg_more_act5, axis=0), color='olivedrab', alpha=0.2,)
plt.fill_between(xran_full5, Tavg_less_act5.mean(axis=0) - stats.sem(Tavg_less_act5, axis=0), Tavg_less_act5.mean(axis=0) + stats.sem(Tavg_less_act5, axis=0), color='royalblue', alpha=0.2,)
plt.fill_between(xran_full5, Tavg_removed_data5.mean(axis=0) - stats.sem(Tavg_removed_data5, axis=0), Tavg_removed_data5.mean(axis=0) + stats.sem(Tavg_removed_data5, axis=0), color='dimgrey', alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--')
# plt.axvspan(0,0.5,facecolor='gainsboro', alpha=0.5)
plt.xlabel('Time(s)', fontsize=14)
plt.ylim(-0.03,0.03)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.ylabel('Normalised Δf/f', fontsize=14)
plt.legend(["Excited responsive (N=16)","Suppressed responsive (N=42)","Unresponsive (N=142)", "Stimulus onset"], loc='upper left', fontsize=14)
plt.title("Means of responsive and unresponsive ROIs at stimulus onset on day 5 where mouse CTBD11.1i received HCl/Sucrose / AX")
plt.show()

#############################################


