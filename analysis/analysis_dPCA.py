from dPCA import dPCA
import matplotlib.pyplot as plt
import numpy as np
import session
from assoc_utils import assoc_ml_util

datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_df = "/Users/sonalisriranga/PycharmProjects/sonalisr/mouse_df.csv"

mouse_n = 1
date_list = [20190131]
area_list = [1]

sess_list = []
roi_data_list = []
xran_list = []
Tavg_data = []
resp = []
for i, date in enumerate(date_list):
    # Creating session objects:
    sess = session.Session(datadir, area_list[i], date = date, mouse_n = mouse_n, mouse_df=mouse_df)
    sess_list.append(sess)
    # Get roi traces
    xran, roi_data = assoc_ml_util.roi_traces_by_trial_criteria(sess)
    roi_data_list.append(roi_data)
    xran_list.append(xran)
    # Trial average roi_data to create a 2-D matrix
    tavg_data = np.mean(roi_data, axis=1)
    Tavg_data.append(tavg_data)

dpca = dPCA(labels='st', regularizer='auto')
