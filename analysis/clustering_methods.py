import matplotlib.pyplot as plt
import numpy as np
from sklearn import metrics
from sklearn.cluster import DBSCAN, KMeans
from sklearn.decomposition import PCA
from sklearn_extra.cluster import KMedoids
import session
from assoc_utils import assoc_ml_util
import seaborn as sns
import sklearn.cluster as cluster
import time
sns.set_context('poster')
sns.set_color_codes()
plot_kwds = {'alpha' : 0.5, 's' : 60, 'linewidths':-1}

# Set paths to data directory and mouse dataframe
datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_df = "/Users/sonalisriranga/PycharmProjects/sonalisr/mouse_df.csv"

'''
This file is merely exploratory.
Use the code below to try out the different clustering algorithms.
Change the parameters and play! 
'''


# Add mouse number, dates and area number
# Note: Here, area number refers to session number

mouse_n = 2
date_list = [20190131,20190131,20190131]
area_list = [1, 2, 3]

sess_list = []
roi_data_list = []
xran_list = []
Tavg_data = []
resp = []

# This iterates through the date lists, creates session objects, ROI data (neuron x trial x time) and
# the output is Trial-averaged traces (neurons x time)
for i, date in enumerate(date_list):
    # Creating session objects:
    sess = session.Session(datadir, area_list[i], date = date, mouse_n = mouse_n, mouse_df=mouse_df)
    sess_list.append(sess)
    # roi traces
    xran, roi_data = assoc_ml_util.roi_traces_by_trial_criteria(sess)
    roi_data_list.append(roi_data)
    xran_list.append(xran)
    # Trial average roi_data to create a 2-D matrix
    tavg_data = np.mean(roi_data, axis=1)
    Tavg_data.append(tavg_data)

xran = xran_list[0]

# Get the minimum number of neurons
min_neurons = 100000
for s, session in enumerate(Tavg_data):
    if min_neurons > session.shape[0]:
        min_neurons = session.shape[0]

for s, session in enumerate(Tavg_data):
    cut_session = session[0:min_neurons,:]
    max = np.max(cut_session)
    Tavg_data[s] = cut_session/max

# concatenate trial averaged data of multiple sessions
full_data = np.concatenate(Tavg_data, axis=1)
reduced_data1 = PCA(n_components=2).fit_transform(full_data)
###############################################################
# Plotting different clustering algorithms
def plot_clusters(data, algorithm, args, kwds):
    start_time = time.time()
    labels = algorithm(*args, **kwds).fit_predict(data)
    end_time = time.time()
    palette = sns.color_palette('dark', np.unique(labels).max() + 1)
    colors = [palette[x] if x >= 0 else (0.0, 0.0, 0.0) for x in labels]
    plt.scatter(data.T[0], data.T[1], c=colors, **plot_kwds)
    frame = plt.gca()
    frame.axes.get_xaxis().set_visible(False)
    frame.axes.get_yaxis().set_visible(False)
    plt.title('Clusters found by {}'.format(str(algorithm.__name__)), fontsize=15)


f= plt.subplots(figsize=(15,15))
plt.subplot(7,2,1)
plot_clusters(reduced_data1, cluster.KMeans, (), {'n_clusters':3, 'n_init':20, 'algorithm':'full'})

plt.subplot(7,2,2)
plot_clusters(reduced_data1, cluster.AffinityPropagation, (), {'preference':-0.4, 'damping':0.55})

plt.subplot(7,2,3)
plot_clusters(reduced_data1, cluster.MeanShift, (1,), {'cluster_all':True})

plt.subplot(7,2,4)
plot_clusters(reduced_data1, cluster.SpectralClustering, (), {'n_clusters':3})

plt.subplot(7,2,5)
plot_clusters(reduced_data1, cluster.AgglomerativeClustering, (), {'n_clusters':3, 'linkage':'ward'})

plt.subplot(7,2,6)
plot_clusters(reduced_data1, cluster.DBSCAN, (), {'eps':0.2})

plt.subplot(7,2,7)
plot_clusters(reduced_data1, KMedoids, (), {'metric':'manhattan', 'n_clusters':3})

plt.show()

#########################
# Plot the decision boundary. This gives you a glimpse of how the clustering is applied.

reduced_data = PCA(n_components=2).fit_transform(full_data)
h = 0.02
x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

plt.figure()
plt.clf()

plt.suptitle(
    "Comparing multiple K-Medoids metrics to K-Means and each other",
    fontsize=14,
)


selected_models = [
    (
        KMedoids(metric="manhattan", n_clusters=6),
        "KMedoids (manhattan)",
    ),
    (
        KMedoids(metric="euclidean", n_clusters=6),
        "KMedoids (euclidean)",
    ),
    (KMedoids(metric="cosine", n_clusters=6), "KMedoids (cosine)"),

    (KMeans(n_clusters=6), "KMeans"),
]

plot_rows = int(np.ceil(len(selected_models) / 2.0))
plot_cols = 2

for i, (model, description) in enumerate(selected_models):

    # Obtain labels for each point in mesh. Use last trained model.
    model.fit(reduced_data)
    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.subplot(plot_cols, plot_rows, i + 1)
    plt.imshow(
        Z,
        interpolation="nearest",
        extent=(xx.min(), xx.max(), yy.min(), yy.max()),
        cmap=plt.cm.Paired,
        aspect="auto",
        origin="lower",
    )

    plt.plot(
        reduced_data[:, 0], reduced_data[:, 1], "k.", markersize=10, alpha=0.5
    )
    # Plot the centroids as a white X
    centroids = model.cluster_centers_
    plt.scatter(
        centroids[:, 0],
        centroids[:, 1],
        marker="x",
        s=90,
        linewidths=1,
        color="w",
        zorder=10,
    )
    plt.title(description)
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())

plt.show()