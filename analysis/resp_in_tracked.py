import numpy as np
from matplotlib.ticker import MultipleLocator

import session
import matplotlib.pyplot as plt
from scipy import stats
from assoc_utils import assoc_ml_util, assoc_gen_util
import pandas as pd
import seaborn as sns

mouse_id = 'CTBD11.1i'
# Set paths to data directory and mouse dataframe
datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_df = "/Users/sonalisriranga/PycharmProjects/sonalisr/mouse_df.csv"
roi_file = "/Users/sonalisriranga/PycharmProjects/sonalisr/roi_tracking/%s/consitent_rois_plane1_all_all.csv" % mouse_id

date_list = [20190131,20190131,20190131,20190201,20190201,20190202,20190202,20190203,20190203,20190204,20190204,
             20190204,20190204]
area_list = [1, 2, 3, 1, 2, 1, 2, 1, 2, 1, 2, 3, 4]
sess_list = []
resp_data_list = []
tracked_data_list = []
xran_list = []
Tavg_data = []
resp_ns_list = []
common_list = []
track_ns_list = []

for i, date in enumerate(date_list):
    # Creating session objects:
    sess = session.Session(datadir, area_list[i], date = date, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
    # List of session objects
    sess_list.append(sess)
    # Gets the tracked rois for each session
    xran_tracked, tracked_rois = assoc_ml_util.tracked_roi_traces(sess)
    # Gets the responsive rois for each session
    resp_rois, xran_resp, full_data = assoc_ml_util.responsive_traces(sess)
    # List of resp roi traces with shape ( n x t x f )
    resp_data_list.append(resp_rois)
    # List of tracked roi traces with shape ( n x t x f )
    tracked_data_list.append(tracked_rois)
    # List of xran list for resp rois
    xran_list.append(xran_resp)
    # Gets the index of the rois that are responsive
    resp_ns = np.where(sess_list[i].resp_rois)[0]
    # List with the responsive rois for each session
    resp_ns_list.append(resp_ns)
    # Gets a series of intex number for the tracked rois for each session
    Track_ns = sess_list[i].tracked_rois
    # List with the tracked rois for each session
    track_ns_list.append(Track_ns)
    # Matches the index of tracked and responsive rois to find the ones they have in common
    s = set(track_ns_list[i]) & set(resp_ns_list[i])
    # List of the common indices i.e., index of tracked rois that were responsive
    common_list.append(s)


respn = np.where(sess_list[1].resp_rois)[0]
Track_ns = sess_list[1].tracked_rois
tryyyyy2 = set(track_ns_list[1]).intersection(resp_ns_list[1])

# Number of licks per reward delivery for both mice

lickwindow1 = [51, 32, 34, 34, 43,45, 44, 43, 44, 44, 33, 32, 33, 33, ]
licks1 = [48, 32, 34, 33, 43, 43, 44, 43, 44, 43, 33, 32, 32,14]
lickpercentage1 = [96, 100, 100, 97, 100, 96, 100,100, 100, 98, 100,100, 97, 42 ]

lickwindows2 = [50, 33,34,35,42,43,42, 43, 43, 42, 32, 33, 34, 34 ]
licks2 = [49, 33, 34, 35, 5, 42,41, 43, 43, 42,32, 33, 34,34 ]
lickpercentage2 = [98, 100, 100, 100, 12, 98, 98, 100, 100,100,100,100,98,100 ]
day = ['A1', 'B1', 'Y1', 'Y2', 'AX2','Y3', 'AX3','Y4', 'AX4','Y5', 'A5', 'X5', 'B5']

# Plotting licks
fig, ax = plt.subplots()
ax.plot(lickpercentage1, linestyle='-', marker='o', color='crimson', label= 'Mouse one' )
ax.plot(lickpercentage2, linestyle='-', marker='o', color='black', label='Mouse two')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.legend()
plt.xlabel(day)
plt.ylabel('Percentage of Correct Responses')
plt.xlim(-0.5,13)
plt.setp(ax.get_xticklabels(minor=True), visible=True)
plt.axes().xaxis.set_tick_params(which='minor')
plt.show()

