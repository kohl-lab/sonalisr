import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import session
from assoc_utils import assoc_ml_util
import seaborn as sns
sns.set()
sns.set_style("white", {})

# This loads the stable cluster codes
########################################
c1_d1 = pd.read_csv("c1_d1.csv")
c1_d1 = np.asarray(c1_d1.T)

c1_d2 = pd.read_csv("c1_d2.csv")
c1_d2 = np.asarray(c1_d2.T)

c1_d3 = pd.read_csv("c1_d3.csv")
c1_d3 = np.asarray(c1_d3.T)

########################################
c2_d1 = pd.read_csv("c2_d1.csv")
c2_d1 = np.asarray(c2_d1.T)

c2_d2 = pd.read_csv("c2_d2.csv")
c2_d2 = np.asarray(c2_d2.T)

c2_d3 = pd.read_csv("c2_d3.csv")
c2_d3 = np.asarray(c2_d3.T)

########################################
c3_d1 = pd.read_csv("c3_d1.csv")
c3_d1 = np.asarray(c3_d1.T)

c3_d2 = pd.read_csv("c3_d2.csv")
c3_d2 = np.asarray(c3_d2.T)

c3_d3 = pd.read_csv("c3_d3.csv")
c3_d3 = np.asarray(c3_d3.T)

clusters3 = pd.read_csv("clustering_D3")
clustering_D3 = np.asarray(c3_d3)

sns.set_style("white", {})
from scipy import stats
import random
# random.seed(30)
mouse_id = 'CTBD11.1i'
# Set paths to data directory and mouse dataframe
datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_df = "/Users/sonalisriranga/PycharmProjects/sonalisr/mouse_df.csv"
roi_file = "/Users/sonalisriranga/PycharmProjects/sonalisr/roi_tracking/%s/consitent_rois_plane1_ax234.csv" % mouse_id

# datadir = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - " \
#           "SONALISR/2p_processed_data/pipeline_output/imaging/"
# mouse_df = "/Users/anacarolinabotturabarros/PycharmProjects/sonalisr/mouse_df.csv"

n_clusters = 3
date1 = 20190131
area1 = 1
date2 = 20190201
area2 = 2
date3 = 20190202
area3 = 2
date4 = 20190203
area4 = 2
date5 = 20190204
area5 = 1
# Getting session for Y Day 1
sess0 = session.Session(datadir, area=area1, date=date1, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
sess = session.Session(datadir, area=area2, date=date2, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
xran, roi_data = assoc_ml_util.tracked_roi_traces(sess)
tavg_data = np.mean(roi_data, axis=1)
max = np.max(roi_data)
Tavg_data = tavg_data/max

##################################################################
# Getting session for Y Day 2
sess2 = session.Session(datadir, area=area3, date=date3, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
xran2, roi_data2 = assoc_ml_util.tracked_roi_traces(sess2)
tavg_data2 = np.mean(roi_data2, axis=1)
max2 = np.max(roi_data2)
Tavg_data2 = tavg_data2/max2

##################################################################
# Getting session for Y Day 3
sess3 = session.Session(datadir, area=area4, date=date4, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
xran3, roi_data3 = assoc_ml_util.tracked_roi_traces(sess3)
tavg_data3 = np.mean(roi_data3, axis=1)
max3 = np.max(roi_data3)
Tavg_data3 = tavg_data3/max3

##################################################################
# # Getting session for Y Day 4
# sess4 = session.Session(datadir, area=area4, date=date4, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
# xran4, roi_data4 = assoc_ml_util.tracked_roi_traces(sess4)
# tavg_data4 = np.mean(roi_data4, axis=1)
# max4 = np.max(roi_data4)
# Tavg_data4 = tavg_data4/max4


# # ##################################################################
# # Getting session for Y Day 5
# sess5 = session.Session(datadir, area=area5, date=date5, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
# xran5, roi_data5 = assoc_ml_util.tracked_roi_traces(sess5)
# tavg_data5 = np.mean(roi_data5, axis=1)
# max5 = np.max(roi_data5)
# Tavg_data5 = tavg_data5/max5

# # ##################################################################

c1_avg_day1 = np.mean(c1_d1, axis=0)
c2_avg_day1 = np.mean(c2_d1, axis=0)
c3_avg_day1 = np.mean(c3_d1, axis=0)

c1_avg_day2 = np.mean(c1_d2, axis=0)
c2_avg_day2 = np.mean(c2_d2, axis=0)
c3_avg_day2 = np.mean(c3_d2, axis=0)

c1_avg_day3 = np.mean(c1_d3, axis=0)
c2_avg_day3 = np.mean(c2_d3, axis=0)
c3_avg_day3 = np.mean(c3_d3, axis=0)

####################################################################


colours = ['#FF8C00', '#FFD700', '#FF4500']
colours2 = [ '#FF8C00', '#FFD700', '#FF4500']
# AX (day two) clusters applied on A (day five) and X (day five)
# AX (day two), A (day five) and X (day five) computed and clustered together
fig_B, axes = plt.subplots(figsize=(15,15))
plt.subplots_adjust(wspace=0.3, hspace=0.4)

ax2 = plt.subplot(3,2,1)
plt.plot(xran[60:120], c1_avg_day1, color=colours[0])
plt.fill_between(xran, c1_avg_day1 - stats.sem(c1_d1, axis=0),
                 c1_avg_day1 + stats.sem(c1_d1, axis=0), color=colours[0], alpha=0.2,)
plt.plot(xran[60:120], c2_avg_day1, color=colours[1])
plt.fill_between(xran, c2_avg_day1 - stats.sem(c2_d1, axis=0),
                 c2_avg_day1 + stats.sem(c2_d1, axis=0), color=colours[1], alpha=0.2,)
plt.plot(xran[60:120], c3_avg_day1, color=colours[2])
plt.fill_between(xran, c3_avg_day1 - stats.sem(c3_d1, axis=0),
                 c3_avg_day1 + stats.sem(c3_d1, axis=0), color=colours[2], alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--')
ax2.set_xlabel('')
plt.ylim(-0.04,0.04)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)


ax4 = plt.subplot(3,2,2)
plt.plot(xran[60:120], c1_avg_day2, color=colours[0])
plt.fill_between(xran, c1_avg_day2 - stats.sem(c1_d2, axis=0),
                 c1_avg_day2 + stats.sem(c1_d2, axis=0), color=colours[0], alpha=0.2,)
plt.plot(xran[60:120], c2_avg_day2, color=colours[1])
plt.fill_between(xran, c2_avg_day2 - stats.sem(c2_d2, axis=0),
                 c2_avg_day2 + stats.sem(c2_d2, axis=0), color=colours[1], alpha=0.2,)
plt.plot(xran[60:120], c3_avg_day2, color=colours[2])
plt.fill_between(xran, c3_avg_day2 - stats.sem(c3_d2, axis=0),
                 c3_avg_day2 + stats.sem(c3_d2, axis=0), color=colours[2], alpha=0.2,)
plt.title('2')
plt.ylabel('Normalised dF/F')
plt.xlabel('')
plt.ylim(-0.04,0.04)
plt.axvline(0, color='silver', linestyle='--')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

ax6 = plt.subplot(3,2,3)
plt.plot(xran[60:120], c1_avg_day3, color=colours[0])
plt.fill_between(xran, c1_avg_day3 - stats.sem(c1_d3, axis=0),
                 c1_avg_day3 + stats.sem(c1_d3, axis=0), color=colours[0], alpha=0.2,)
plt.plot(xran[60:120], c2_avg_day3, color=colours[1])
plt.fill_between(xran, c2_avg_day3 - stats.sem(c2_d3, axis=0),
                 c2_avg_day3 + stats.sem(c2_d3, axis=0), color=colours[1], alpha=0.2,)
plt.plot(xran[60:120], c3_avg_day3, color=colours[2])
plt.fill_between(xran, c3_avg_day3 - stats.sem(c3_d3, axis=0),
                 c3_avg_day3 + stats.sem(c3_d3, axis=0), color=colours[2], alpha=0.2,)
plt.title('3')
# plt.xlabel('Time(s)')
ax6.set_ylabel('')
plt.ylim(-0.04,0.04)
plt.axvline(0, color='silver', linestyle='--')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.show()
