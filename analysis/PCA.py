import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
sns.set_style("white")
import numpy as np
from sklearn.decomposition import PCA
from mpl_toolkits.mplot3d import Axes3D
from sklearn import metrics
import session
from assoc_utils import assoc_ml_util


mouse_id = 'CTBD7.1d'
# mouse_id = 'CTBD11.1i'

# Set paths to data directory and mouse dataframe
datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_df = "/Users/sonalisriranga/PycharmProjects/sonalisr/mouse_df.csv"
# roi_file = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/rois_analysis" \
#            "/%s/rois_mapping/consitent_rois_plane1_y_all.csv" % mouse_id
roi_file = "/Users/sonalisriranga/PycharmProjects/sonalisr/roi_tracking/%s/consitent_rois_plane1_b15.csv" % mouse_id

# datadir = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - " \
#           "SONALISR/2p_processed_data/pipeline_output/imaging/"
# mouse_df = "/Users/anacarolinabotturabarros/PycharmProjects/sonalisr/mouse_df.csv"
# roi_file = "/Users/anacarolinabotturabarros/PycharmProjects/sonalisr/roi_tracking/%s/" \
#            "consitent_rois_plane1.csv" % mouse_id

# Set mouse id, area and date
date_list = [20190131, 20190131,20190131]
# date_list = [20190204]
area_list = [1,2,3]
# area_list = [4]
sess_list = []
roi_data_list = []
xran_list = []
Tavg_data = []
resp = []

for i, date in enumerate(date_list):
    # Creating session objects:
    sess = session.Session(datadir, area_list[i], date = date, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
    sess_list.append(sess)
    # Use this for non-tracked ROIS
    xran, roi_data = assoc_ml_util.roi_traces_by_trial_criteria(sess)
    # Use this for tracked rois to compare multiple sessions
    # xran, roi_data = assoc_ml_util.tracked_roi_traces(sess)
    roi_data_list.append(roi_data)
    xran_list.append(xran)
    # Trial average roi_data to create a 2-D matrix
    tavg_data = np.mean(roi_data, axis=1)
    Tavg_data.append(tavg_data)

xran = xran_list[0]

# plotting neurons from raw traces
# f2 = plt.figure(2)
# for i in range(0, len(roi_data_list[0][:,0,0])):
#     plt.plot(xran, roi_data_list[0][i, 0, :])
# plt.show()

# Get the minimum number of neurons
min_neurons = 100000
for s, session in enumerate(Tavg_data):
    if min_neurons > session.shape[0]:
        min_neurons = session.shape[0]

for s, session in enumerate(Tavg_data):
    cut_session = session[0:min_neurons,:]
    max = np.max(cut_session)
    Tavg_data[s] = cut_session/max

# concatenate trial averaged data of multiple sessions
# full_data = np.concatenate(Tavg_data, axis=1)
# Use this for tracked rois for mouse 2 to excluse session 3 from analysis:
full_data = np.concatenate((Tavg_data[1],Tavg_data[2]), axis=1)
##############################################################
# Computing Principal Component Analysis
n_comp = 20
print('Running PCA with {} components'.format(n_comp))
pcamodel = PCA(n_components=n_comp)
pca = pcamodel.fit_transform(full_data)
pcs = pca.T

# # Percentage of variance explained by each of the selected components:
print('Percentage of variance explained by each of the selected components: {}'
      .format(pcamodel.explained_variance_ratio_))
#
# # Amount of variance explained by each of the selected components:
print('Amount of variance explained by each of the selected components: {}'
      .format(pcamodel.explained_variance_))

#cumulative sum of variance explained with [n] features
var=np.cumsum(np.round(pcamodel.explained_variance_ratio_, decimals=3)*100)
cumulative_v = np.cumsum(np.round(pcamodel.explained_variance_, decimals=3)*100)


fig_A = plt.figure('Bs_vr')
plt.ylabel('Cumulative Explained Variance')
plt.xlabel('Number of Components')
plt.title('Explained variance by components')
plt.ylim(30,100)
plt.style.context('seaborn-whitegrid')
plt.plot(var, linestyle='-', marker='o', color='black')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
# plt.show()

# PC where >95% variance explained
np.argmax(var >= 0.95) + 1
# Variance explained with first 2 PCs
print('First two principal components explain {}% of the variance.'
      .format(np.round(var[1], decimals=3)))
print('TFirst three principal components explain {}% of the variance.'
      .format(np.round(var[2], decimals=3)))

#############################################################
# (2) Principal components plotted over each other in a scatter plot
fig_B = plt.figure('Bs_scatter')
ax1 = plt.subplot(1,3,1)
sns.scatterplot(pcs[0,:], pcs[1,:], color='#6B8E23', alpha=0.75, s=90)
plt.xlabel('Principal Component 1')
plt.ylabel('Principal Component 2')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)

ax2 = plt.subplot(1,3,2)
sns.scatterplot(pcs[1,:], pcs[2,:], color='#6B8E23', alpha=0.75, s=90)
plt.xlabel('Principal Component 2')
plt.ylabel('Principal Component 3')
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.title('Projection of the first three principal components', fontsize=12)

ax3 = plt.subplot(1,3,3)
sns.scatterplot(pcs[2,:], pcs[0,:], color='#6B8E23', alpha=0.75, s=90)
plt.xlabel('Principal Component 1')
plt.ylabel('Principal Component 3')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
# plt.show()

##############################################################
# (3) Principal components projected over time
proj = pcs.dot(full_data)
txt='* Explained variance by first three principal components: {}% .'.format(np.round(var[2], decimals=3))
fig_C = plt.figure('Bs_overtime')
fig_C.text(.6, .05, txt, ha='left', fontsize=10)
ax1 = plt.subplot(3,1,1)
sns.lineplot(xran, proj[0,0:120], color='#6B8E23')
sns.lineplot(xran, proj[0,120:240], color= '#006400')
# sns.lineplot(xran, proj[0,240:360], color='#FFFF00')
# sns.lineplot(xran, proj[0,360:480], color='mediumblue')
# sns.lineplot(xran, proj[0,480:600], color='teal')
# sns.lineplot(xran, proj[0,480:600], color='cyan')
plt.axvline(0, color='silver', linestyle='--', label='Stimulus onset')
plt.title('Tracked ROIs for Bs / Saline projected along eigenvector direction')
# plt.xlabel('Time')
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.ylabel('Principal Component 1')
plt.legend()
# plt.legend(['D1', 'D2', 'D3', 'D4', 'D5', 'Stimulus Onset'], bbox_to_anchor=(1.10, 1), loc='upper right', borderaxespad=0.)
# # plt.legend(['D1','D2','Stim Start'])
# # plt.legend(['Y', 'AX','X','A','Stim Start'])
# # plt.legend(['Y','A','B','Stim Start'], loc=2)
# # plt.legend(['AX','Y','Stim Start'])
plt.legend(['Day1: B','Day5: B','Stimulus Onset'])
#
ax2 = plt.subplot(3,1,2)
sns.lineplot(xran, proj[1,0:120], color='#6B8E23')
sns.lineplot(xran, proj[1,120:240], color= '#006400')
# sns.lineplot(xran, proj[1,240:360], color='#FFFF00')
# sns.lineplot(xran, proj[1,360:480], color='mediumblue')
# sns.lineplot(xran, proj[1,480:600], color='teal')
# sns.lineplot(xran, proj[1,480:600], color='cyan')
plt.axvline(0, color='silver', linestyle='--')
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
# plt.xlabel('Time')
plt.ylabel('Principal Component 2')
#
ax3 = plt.subplot(3,1,3)
sns.lineplot(xran, proj[2,0:120], color='#6B8E23')
sns.lineplot(xran, proj[2,120:240], color= '#006400')
# sns.lineplot(xran, proj[2,240:360], color='#FFFF00')
# sns.lineplot(xran, proj[2,360:480], color='mediumblue')
# sns.lineplot(xran, proj[2,480:600], color='teal')
# sns.lineplot(xran, proj[2,480:600], color='cyan')
plt.axvline(0, color='silver', linestyle='--')
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.xlabel('Time(s)')
plt.ylabel('Principal Component 3')

# ax4 = plt.subplot(6,2,4)
# sns.lineplot(xran, proj[3,0:120], color='skyblue')
# # # plt.plot(xran, proj[3,120:240], color='skyblue')
# # # plt.plot(xran, proj[3,240:360], color='royalblue')
# # # plt.plot(xran, proj[3,360:480], color='highseagreen')
# # # plt.plot(xran, proj[3,480:600], color='lightskyblue')
# # plt.axvline(0, color='silver', linestyle='--')
# ax4.spines['right'].set_visible(False)
# ax4.spines['top'].set_visible(False)
# plt.gca().spines['top'].set_visible(False)
# plt.gca().spines['right'].set_visible(False)
# # plt.xlabel('Time(s)')
# # plt.ylabel('PC4')
# #
# plt.subplot(6,2,5)
# sns.lineplot(xran, proj[4,0:120], color='deepskyblue')
# # # # plt.plot(xran, proj[4,120:240], color='r')
# # # # plt.plot(xran, proj[4,240:360], color='b')
# # # # # plt.plot(xran, proj[2,360:480], color='m')
# # # # # plt.plot(xran, proj[2,480:600], color='g')
# plt.axvline(0, color='silver', linestyle='--')
# # # plt.xlabel('Time')
# # plt.ylabel('PC5')
# # #
# plt.subplot(6,2,6)
# sns.lineplot(xran, proj[5,0:120], color='deepskyblue')
# # # # plt.plot(xran, proj[5,120:240], color='r')
# # # # plt.plot(xran, proj[5,240:360], color='b')
# # # # # plt.plot(xran, proj[2,360:480], color='m')
# # # # # plt.plot(xran, proj[2,480:600], color='g')
# # plt.axvline(0, color='silver', linestyle='--')
# # plt.xlabel('Time')
# # plt.ylabel('PC6')
plt.setp(ax1.get_xticklabels(), visible=False)
plt.setp(ax2.get_xticklabels(), visible=False)
# plt.setp(ax3.get_xticklabels(), visible=False)
# plt.show()


##############################################################
# Plotting PCA along a three-dimensional line
# fig_D = plt.figure('Bs_3D')
# ax = plt.axes(projection='3d')
# zline = proj[0,0:120]
# xline = proj[1,0:120]
# yline = proj[2,0:120]
# ax.plot3D(xline, yline, zline, '#6B8E23')
#
# zline = proj[0,120:240]
# xline = proj[1,120:240]
# yline = proj[2,120:240]
# ax.plot3D(xline, yline, zline,  '#006400')


# zline = proj[0,240:360]
# xline = proj[1,240:360]
# yline = proj[2,240:360]
# ax.plot3D(xline, yline, zline, '#FFFF00')
#
# zline = proj[0,360:480]
# xline = proj[1,360:480]
# yline = proj[2,360:480]
# ax.plot3D(xline, yline, zline, 'mediumblue')
# # #
# zline = proj[0,480:600]
# xline = proj[1,480:600]
# yline = proj[2,480:600]
# ax.plot3D(xline, yline, zline, 'teal')
#
# ax.legend(['D1', 'D2', 'D3', 'D4', 'D5'])
# ax.legend(['Day1: B','Day5: B'])
# ax.legend(['Y', 'AX', 'X', 'A'])
# ax.legend(['Y','A','B','Stim Start'])
# ax.legend(['D1', 'D5'])
# ax.legend(['Y', 'A', 'B'])
# ax.legend(['AX', 'Y'])
# ax.legend(['Day1: Y','Day1: A','Day1: B'])
# ax.legend(['Y', 'AX'])
# plt.show()



