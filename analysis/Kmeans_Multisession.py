import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
import session
from assoc_utils import assoc_ml_util
import seaborn as sns
sns.set()
import random
random.seed(1111)
from statsmodels.stats.anova import AnovaRM
from statsmodels.stats.multicomp import pairwise_tukeyhsd
import statsmodels.api as sm
from statsmodels.formula.api import ols

sns.set_style("white", {})
from scipy import stats
import random
# random.seed(30)
mouse_id = 'CTBD7.1d'
# Set paths to data directory and mouse dataframe
datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_df = "/Users/sonalisriranga/PycharmProjects/sonalisr/mouse_df.csv"
roi_file = "/Users/sonalisriranga/PycharmProjects/sonalisr/roi_tracking/%s/consitent_rois_plane1_a1_ax2_x5.csv" % mouse_id

# datadir = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - " \
#           "SONALISR/2p_processed_data/pipeline_output/imaging/"
# mouse_df = "/Users/anacarolinabotturabarros/PycharmProjects/sonalisr/mouse_df.csv"

n_clusters = 2
date1 = 20190131
area1 = 1
date2 = 20190201
area2 = 1
date3 = 20190204
area3 = 1
date4 = 20190203
area4 = 2
date5 = 20190204
area5 = 3
# Getting session for Y Day 1
# sess0 = session.Session(datadir, area=area1, date=date1, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
sess = session.Session(datadir, area=area1, date=date1, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
xran, roi_data = assoc_ml_util.tracked_roi_traces(sess)
tavg_data = np.mean(roi_data, axis=1)
max = np.max(roi_data)
Tavg_data = tavg_data/max


reduced_data1 = PCA(n_components=15).fit_transform(Tavg_data)
kmeans1 = KMeans(init='k-means++', n_clusters=n_clusters, n_init=10)
clusters1 = kmeans1.fit_predict(reduced_data1)
centroids1 = kmeans1.cluster_centers_
reduced_data1 = reduced_data1.T


# Converting it into a df and adding columns with clusters and their labels
df_pca_clusters1 = pd.DataFrame(reduced_data1.T)
df_pca_clusters1['Clusters_ns'] = clusters1
df_pca_clusters1['Clusters'] = df_pca_clusters1['Clusters_ns'].map({
    0: 'First',
    1: 'Second',
    # 2: 'Third'
})

print(clusters1.shape)


##################################################################
# Getting session for Y Day 2
sess2 = session.Session(datadir, area=area2, date=date2, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
xran2, roi_data2 = assoc_ml_util.tracked_roi_traces(sess2)
tavg_data2 = np.mean(roi_data2, axis=1)
max2 = np.max(roi_data2)
Tavg_data2 = tavg_data2/max2


reduced_data2 = PCA(n_components=15).fit_transform(Tavg_data2)
kmeans2 = KMeans(init='k-means++', n_clusters=n_clusters, n_init=10)
clusters2 = kmeans2.fit_predict(reduced_data2)
centroids2 = kmeans2.cluster_centers_
reduced_data2 = reduced_data2.T

df_pca_clusters2 = pd.DataFrame(reduced_data2.T)
df_pca_clusters2['Clusters_ns'] = clusters2
df_pca_clusters2['Clusters'] = df_pca_clusters2['Clusters_ns'].map({
    0: 'First',
    1: 'Second',
    # 2: 'Third'
})

print(clusters2.shape)
##################################################################
# Getting session for Y Day 3
sess3 = session.Session(datadir, area=area3, date=date3, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
xran3, roi_data3 = assoc_ml_util.tracked_roi_traces(sess3)
tavg_data3 = np.mean(roi_data3, axis=1)
max3 = np.max(roi_data3)
Tavg_data3 = tavg_data3/max3

# Comment this out for AX and Ax,A & X analysis
reduced_data3 = PCA(n_components=15).fit_transform(Tavg_data3)
kmeans3 = KMeans(init='k-means++', n_clusters=n_clusters, n_init=10)
clusters3 = kmeans3.fit_predict(reduced_data3)
centroids3 = kmeans3.cluster_centers_
reduced_data3 = reduced_data3.T

clustering_D3 = pd.DataFrame(data=clusters3)
clustering_D3.to_csv('clustering_D3', index=False)

df_pca_clusters3 = pd.DataFrame(reduced_data3.T)
df_pca_clusters3['Clusters_ns'] = clusters3
df_pca_clusters3['Clusters'] = df_pca_clusters3['Clusters_ns'].map({
    0: 'First',
    1: 'Second',
    # 2: 'Third'
})

print(clusters3.shape)
##################################################################
# # Getting session for Y Day 4
# sess4 = session.Session(datadir, area=area4, date=date4, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
# xran4, roi_data4 = assoc_ml_util.tracked_roi_traces(sess4)
# tavg_data4 = np.mean(roi_data4, axis=1)
# max4 = np.max(roi_data4)
# Tavg_data4 = tavg_data4/max4
# print(Tavg_data.shape)
# # Y4_full_data = np.concatenate(Tavg_data4, axis=0)

# df_pca_clusters4 = pd.DataFrame(reduced_data4.T)
# df_pca_clusters4['Clusters_ns'] = clusters4
# df_pca_clusters4['Clusters'] = df_pca_clusters4['Clusters_ns'].map({
#     0: 'First',
#     1: 'Second',
#     # 2: 'Third'
# })
# # # # ##################################################################
# # Getting session for Y Day 5
# sess5 = session.Session(datadir, area=area5, date=date5, mouseid=mouse_id, mouse_df=mouse_df, roi_file=roi_file)
# xran5, roi_data5 = assoc_ml_util.tracked_roi_traces(sess5)
# tavg_data5 = np.mean(roi_data5, axis=1)
# max5 = np.max(roi_data5)
# Tavg_data5 = tavg_data5/max5
#
#
# reduced_data5 = PCA(n_components=15).fit_transform(Tavg_data5)
# kmeans5 = KMeans(init='k-means++', n_clusters=n_clusters, n_init=10)
# clusters5 = kmeans5.fit_predict(reduced_data5)
# centroids5 = kmeans5.cluster_centers_
# reduced_data5 = reduced_data5.T
#
#
# df_pca_clusters5 = pd.DataFrame(reduced_data5.T)
# df_pca_clusters5['Clusters_ns'] = clusters5
# df_pca_clusters5['Clusters'] = df_pca_clusters5['Clusters_ns'].map({
#     0: 'First',
#     1: 'Second',
#     # 2: 'Third'
# })

######################################################
# Applying  clusters to each day's trial averaged data
# Day one
c1_d1 = Tavg_data[clusters3 == 0, 60:120]
c2_d1 = Tavg_data[clusters3 == 1, 60:120]
# c3_d1 = Tavg_data[clusters3 == 2, 60:120]


# Day two
c1_d2 = Tavg_data2[clusters3 == 0, 60:120]
c2_d2 = Tavg_data2[clusters3 == 1, 60:120]
# c3_d2 = Tavg_data2[clusters3 == 2, 60:120]


# Day three
c1_d3 = Tavg_data3[clusters3 == 0, 60:120]
c2_d3 = Tavg_data3[clusters3 == 1, 60:120]
# c3_d3 = Tavg_data3[clusters3 == 2, 60:120]

# Uncomment for flavour Y analysis

# # Day four
# c1_d4 = Tavg_data3[clusters5 == 0, 60:120]
# c2_d4 = Tavg_data3[clusters5 == 1, 60:120]
# c3_d4 = Tavg_data3[clusters5 == 2, 60:120]
#
# # Day five
# c1_d5 = Tavg_data3[clusters5 == 0, 60:120]
# c2_d5 = Tavg_data3[clusters5 == 1, 60:120]
# c3_d5 = Tavg_data3[clusters5 == 2, 60:120]


######################################################

c1_day1 = Tavg_data[clusters3 == 0, :]
c2_day1 = Tavg_data[clusters3 == 1, :]
c3_day1 = Tavg_data[clusters3 == 2, :]

c1_avg_day1 = np.mean(c1_day1, axis=0)
c2_avg_day1 = np.mean(c2_day1, axis=0)
c3_avg_day1 = np.mean(c3_day1, axis=0)

# Day two
c1_day2 = Tavg_data2[clusters3 == 0, :]
c2_day2 = Tavg_data2[clusters3 == 1, :]
c3_day2 = Tavg_data2[clusters3 == 2, :]

c1_avg_day2 = np.mean(c1_day2, axis=0)
c2_avg_day2 = np.mean(c2_day2, axis=0)
c3_avg_day2 = np.mean(c3_day2, axis=0)

# Day three
c1_day3 = Tavg_data3[clusters3 == 0, :]
c2_day3 = Tavg_data3[clusters3 == 1, :]
c3_day3 = Tavg_data3[clusters3 == 2, :]

c1_avg_day3 = np.mean(c1_day3, axis=0)
c2_avg_day3 = np.mean(c2_day3, axis=0)
c3_avg_day3 = np.mean(c3_day3, axis=0)


# # Day four
# c1_day4 = Tavg_data4[clusters5 == 0, :]
# c2_day4 = Tavg_data4[clusters5 == 1, :]
# c3_day4 = Tavg_data4[clusters5 == 2, :]
#
# c1_avg_day4 = np.mean(c1_day4, axis=0)
# c2_avg_day4 = np.mean(c2_day4, axis=0)
# c3_avg_day4 = np.mean(c3_day4, axis=0)
#
# # Day five
# c1_day5 = Tavg_data5[clusters5 == 0, :]
# c2_day5 = Tavg_data5[clusters5 == 1, :]
# c3_day5 = Tavg_data5[clusters5 == 2, :]
#
# c1_avg_day5 = np.mean(c1_day5, axis=0)
# c2_avg_day5 = np.mean(c2_day5, axis=0)
# c3_avg_day5 = np.mean(c3_day5, axis=0)

################################################################
# APPLYING ALL THREE DAYS CLUSTERING
# full_data = np.concatenate((Tavg_data2, Tavg_data, Tavg_data3), axis=1)
# full_pca = PCA(n_components=15).fit_transform(full_data)
# kmeans_full = KMeans(init='k-means++', n_clusters=n_clusters, n_init=10)
# clustersfull = kmeans_full.fit_predict(full_pca)
# centroidsfull = kmeans_full.cluster_centers_
# full_pca = full_pca.T
#
# print(clustersfull.shape)
#
# df_pca_full = pd.DataFrame(full_pca.T)
# df_pca_full['Clusters_ns'] = clustersfull
# df_pca_full['Clusters'] = df_pca_full['Clusters_ns'].map({
#     0: 'First',
#     1: 'Second',
#     2: 'Third'
# })
################################################################
# This saves each cluster as a seperate dataframe.
c1_day1_s = pd.DataFrame(data=c1_d1.T)
c2_day1_s = pd.DataFrame(data=c2_d1.T)
# c3_day1_s = pd.DataFrame(data=c3_d1.T)

c1_day1_s.to_csv('c1_d1.csv', index=False)
c2_day1_s.to_csv('c2_d1.csv', index=False)
# c3_day1_s.to_csv('c3_d1.csv', index=False)

c1_day2_s = pd.DataFrame(data=c1_d2.T)
c2_day2_s = pd.DataFrame(data=c2_d2.T)
# c3_day2_s = pd.DataFrame(data=c3_d2.T)

c1_day2_s.to_csv('c1_d2.csv', index=False)
c2_day2_s.to_csv('c2_d2.csv', index=False)
# c3_day2_s.to_csv('c3_d2.csv', index=False)

c1_day3_s = pd.DataFrame(data=c1_d3.T)
c2_day3_s = pd.DataFrame(data=c2_d3.T)
# c3_day3_s = pd.DataFrame(data=c3_d3.T)

c1_day3_s.to_csv('c1_d3.csv', index=False)
c2_day3_s.to_csv('c2_d3.csv', index=False)
# c3_day3_s.to_csv('c3_d3.csv', index=False)

# Uncomment for Flavour Y analysis

# c1_day4_s = pd.DataFrame(data=c1_d4.T)
# c2_day4_s = pd.DataFrame(data=c2_d4.T)
# c3_day4_s = pd.DataFrame(data=c3_d4.T)
#
# c1_day4_s.to_csv('c1_d4.csv', index=False)
# c2_day4_s.to_csv('c2_d4.csv', index=False)
# c3_day4_s.to_csv('c3_d4.csv', index=False)
#
# c1_day5_s = pd.DataFrame(data=c1_d5.T)
# c2_day5_s = pd.DataFrame(data=c2_d5.T)
# c3_day5_s = pd.DataFrame(data=c3_d5.T)
#
# c1_day5_s.to_csv('c1_d5.csv', index=False)
# c2_day5_s.to_csv('c2_d5.csv', index=False)
# c3_day5_s.to_csv('c3_d5.csv', index=False)
# df = {'mouse_n': mouse_n, 'activity': neurons, 'cluster': allclusters, 'day': alldays }

# allaxs_df = pd.DataFrame(data=df)

#############################################################################
# Plotting Clusters
colours = ['#FF8C00', '#FFD700', '#FF4500']
colours2 = ['#FF8C00', '#FFD700', '#FF4500']

# AX (day two) clusters applied on A (day five) and X (day five)
# AX (day two), A (day five) and X (day five) computed and clustered together
fig_B, axes = plt.subplots(figsize=(15,15))
plt.subplots_adjust(wspace=0.3, hspace=0.4)
fig_B.suptitle('Visualising the presence of single stimuli representations in compound stimuli (mouse ID: CTBD11.1i) \n'
               '\n'
               ' AX (day two) clusters applied on A (day five) and X (day five)')
ax1 = plt.subplot(3,2,1)
sns.scatterplot(df_pca_clusters1[0], df_pca_clusters1[1],
                    hue=df_pca_clusters3['Clusters'], palette = colours2, s=90, alpha=0.75)
# plt.legend( bbox_to_anchor=(1.05, 1.10),loc='upper right', borderaxespad=0)
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.get_legend().remove()
ax1.set_ylabel('PC2')
ax1.set_xlabel('PC1')

ax3 = plt.subplot(3,2,2)
sns.scatterplot(df_pca_clusters2[0], df_pca_clusters2[1],
                    hue=df_pca_clusters3['Clusters'], palette = colours2, s=90, alpha=0.75)
plt.title('Day five: A')
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
ax3.set_ylabel('PC2')
ax3.set_xlabel('PC2')
ax3.get_legend().remove()

ax5 = plt.subplot(3,2,3)
sns.scatterplot(df_pca_clusters3[0], df_pca_clusters3[1],
                    hue=df_pca_clusters3['Clusters'], palette = colours2, s=90, alpha=0.75)
plt.xlabel('PC1')
plt.title('Day five: X')
ax5.set_ylabel('PC2')
ax5.get_legend().remove()
ax5.spines['right'].set_visible(False)
ax5.spines['top'].set_visible(False)

plt.show()

#############################################################################
colours = ['#FF8C00', '#FFD700']
colours2 = ['#FF8C00', '#FFD700']
# AX (day two) clusters applied on A (day five) and X (day five)
# AX (day two), A (day five) and X (day five) computed and clustered together
fig_B, axes = plt.subplots(figsize=(15,15))
plt.subplots_adjust(wspace=0.3, hspace=0.4)

ax2 = plt.subplot(3,2,1)
plt.plot(xran, c1_avg_day1, color=colours[0])
plt.fill_between(xran, c1_avg_day1 - stats.sem(c1_day1, axis=0),
                 c1_avg_day1 + stats.sem(c1_day1, axis=0), color=colours[0], alpha=0.2,)
plt.plot(xran, c2_avg_day1, color=colours[1])
plt.fill_between(xran, c2_avg_day1 - stats.sem(c2_day1, axis=0),
                 c2_avg_day1 + stats.sem(c2_day1, axis=0), color=colours[1], alpha=0.2,)
# plt.plot(xran, c3_avg_day1, color=colours[2])
# plt.fill_between(xran, c3_avg_day1 - stats.sem(c3_day1, axis=0),
#                  c3_avg_day1 + stats.sem(c3_day1, axis=0), color=colours[2], alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--')
ax2.set_xlabel('')
plt.ylabel('Normalised dF/F')
plt.ylim(-0.03,0.03)
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)


ax4 = plt.subplot(3,2,2)
plt.plot(xran, c1_avg_day2, color=colours[0])
plt.fill_between(xran, c1_avg_day2 - stats.sem(c1_day2, axis=0),
                 c1_avg_day2 + stats.sem(c1_day2, axis=0), color=colours[0], alpha=0.2,)
plt.plot(xran, c2_avg_day2, color=colours[1])
plt.fill_between(xran, c2_avg_day2 - stats.sem(c2_day2, axis=0),
                 c2_avg_day2 + stats.sem(c2_day2, axis=0), color=colours[1], alpha=0.2,)
# plt.plot(xran, c3_avg_day2, color=colours[2])
# plt.fill_between(xran, c3_avg_day2 - stats.sem(c3_day2, axis=0),
#                  c3_avg_day2 + stats.sem(c3_day2, axis=0), color=colours[2], alpha=0.2,)
plt.title('2')
plt.ylabel('')
plt.xlabel('')
plt.ylim(-0.03,0.03)
plt.axvline(0, color='silver', linestyle='--')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

ax6 = plt.subplot(3,2,3)
plt.plot(xran, c1_avg_day3, color=colours[0], label = 'First Cluster')
plt.fill_between(xran, c1_avg_day3 - stats.sem(c1_day3, axis=0),
                 c1_avg_day3 + stats.sem(c1_day3, axis=0), color=colours[0], alpha=0.2)
plt.plot(xran, c2_avg_day3, color=colours[1], label = 'Second Cluster')
plt.fill_between(xran, c2_avg_day3 - stats.sem(c2_day3, axis=0),
                 c2_avg_day3 + stats.sem(c2_day3, axis=0), color=colours[1], alpha=0.2)
# plt.plot(xran, c3_avg_day3, color=colours[2])
# plt.fill_between(xran, c3_avg_day3 - stats.sem(c3_day3, axis=0),
#                  c3_avg_day3 + stats.sem(c3_day3, axis=0), color=colours[2], alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--', label='Stimulus Onset')
plt.title('3')
# plt.xlabel('Time(s)')
ax6.set_ylabel('')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
plt.ylim(-0.03,0.03)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

#
# ax7 = plt.subplot(5,2,4)
# plt.plot(xran, c1_avg_day4, color=colours[0])
# plt.fill_between(xran, c1_avg_day4 - stats.sem(c1_day4, axis=0),
#                  c1_avg_day4 + stats.sem(c1_day4, axis=0), color=colours[0], alpha=0.2,)
# plt.plot(xran, c2_avg_day4, color=colours[1])
# plt.fill_between(xran, c2_avg_day4 - stats.sem(c2_day4, axis=0),
#                  c2_avg_day4 + stats.sem(c2_day4, axis=0), color=colours[1], alpha=0.2,)
# plt.plot(xran, c3_avg_day4, color=colours[2])
# plt.fill_between(xran, c3_avg_day4 - stats.sem(c3_day4, axis=0),
#                  c3_avg_day4 + stats.sem(c3_day4, axis=0), color=colours[2], alpha=0.2,)
# plt.axvline(0, color='silver', linestyle='--')
# plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
# plt.title('3')
# # plt.xlabel('Time(s)')
# ax6.set_ylabel('')
# plt.ylim(-0.05,0.05)
#
# plt.gca().spines['top'].set_visible(False)
# plt.gca().spines['right'].set_visible(False)
#
#
# ax8 = plt.subplot(5,2,5)
# plt.plot(xran, c1_avg_day5, color=colours[0], label = 'First Cluster')
# plt.fill_between(xran, c1_avg_day5 - stats.sem(c1_day5, axis=0),
#                  c1_avg_day5 + stats.sem(c1_day5, axis=0), color=colours[0], alpha=0.2,)
# plt.plot(xran, c2_avg_day5, color=colours[1],  label = 'Second Cluster')
# plt.fill_between(xran, c2_avg_day5 - stats.sem(c2_day5, axis=0),
#                  c2_avg_day5 + stats.sem(c2_day5, axis=0), color=colours[1], alpha=0.2,)
# plt.plot(xran, c3_avg_day5, color=colours[2], label = 'Third Cluster')
# plt.fill_between(xran, c3_avg_day5 - stats.sem(c3_day5, axis=0),
#                  c3_avg_day5 + stats.sem(c3_day5, axis=0), color=colours[2], alpha=0.2,)
# plt.axvline(0, color='silver', linestyle='--', label='Stimulus Onset')
# plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
# plt.title('5')
# # plt.xlabel('Time(s)')
# ax6.set_ylabel('')
# plt.ylim(-0.05,0.05)
#
# plt.gca().spines['top'].set_visible(False)
# plt.gca().spines['right'].set_visible(False)
plt.show()


