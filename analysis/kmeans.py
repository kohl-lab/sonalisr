import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
sns.set_style("white")
from scipy import stats
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
import session
from assoc_utils import assoc_ml_util



mouse_id = 'CTBD7.1d'
mouse_n = 1
# Set paths to data directory and mouse dataframe
datadir = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/pipeline_output/imaging/"
mouse_df = "/Users/sonalisriranga/PycharmProjects/sonalisr/mouse_df.csv"
roi_file = "/Users/sonalisriranga/PycharmProjects/sonalisr/roi_tracking/%s/consitent_rois_plane1_y_all.csv" % mouse_id

# datadir = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - " \
#           "SONALISR/2p_processed_data/pipeline_output/imaging/"
# mouse_df = "/Users/anacarolinabotturabarros/PycharmProjects/sonalisr/mouse_df.csv"
# roi_file = "/Users/anacarolinabotturabarros/PycharmProjects/sonalisr/roi_tracking/%s/" \
#            "consitent_rois_plane1.csv" % mouse_id


# Set mouse number, list of areas and dates
date_list = [20190131]
area_list = [1]
sess_list = []
roi_data_list = []
xran_list = []
Tavg_data = []
resp = []

for i, date in enumerate(date_list):
    sess = session.Session(datadir, area_list[i], date = date, mouse_n = mouse_n, mouse_df=mouse_df, roi_file=roi_file)
    sess_list.append(sess)
    # Use this for non-tracked rois:
    # xran, roi_data = assoc_ml_util.roi_traces_by_trial_criteria(sess)
    # Use this for tracked rois:
    xran, roi_data = assoc_ml_util.tracked_roi_traces(sess)
    roi_data_list.append(roi_data)
    xran_list.append(xran)
    tavg_data = np.mean(roi_data, axis=1)
    Tavg_data.append(tavg_data)
xran = xran_list[0]

# This gives us the minimum number of neurons across sessions to concatenate
min_neurons = 100000
for s, session in enumerate(Tavg_data):
    if min_neurons > session.shape[0]:
        min_neurons = session.shape[0]

for s, session in enumerate(Tavg_data):
    cut_session = session[0:min_neurons,:]
    # Normalise each session by its max value
    max = np.max(cut_session)
    Tavg_data[s] = cut_session/max

# Concatenate trial averaged data of multiple sessions
full_data = np.concatenate(Tavg_data, axis=1)

##############################################################
'''
K-Mean Clustering with PCs 

Objectives: 
    - Run PCA to reduce the dimension of the data we are trying to cluster.
    - Use scree plot (Fig_A) to determine the number of clusters to include.
        For each value of k, we can initialise k_means and use inertia to
        identify the sum of squared distances of samples to the nearest cluster centre.
    - Initialise the k-mean model based on the clusters determined. Here, for individual flavours, the number of 
        clusters are determined by the clustering goodness fit statistic as well as the number of populations identified 
        by the responsive ROI analysis ( done in responsive_roi.py ). 
    - The clusters are plotted with the Principal components in a scatter plot (Fig_B). 
    - The means -+ sem of the identified clusters are projected (Fig_C) along the eigenvectior direction ( time over dF/f).   
'''


# Running PCA with number of components as 20
reduced_data = PCA(n_components=20).fit_transform(full_data)

# Plotting the sum of squared distances against the number of components on a scree plot
# This iterates through the different number of clusters on the x-axis and the sum of squared distances on the y-axis
Sum_of_squared_distances = []
K = range(1,15)
for k in K:
    km = KMeans(n_clusters=k,init='k-means++', n_init=10)
    km = km.fit(reduced_data)
    Sum_of_squared_distances.append(km.inertia_)

# Scree Plot:
fig_A = plt.figure('D5_X_wcss')
plt.plot(K, Sum_of_squared_distances, marker = 'o', linestyle='-', c='black')
plt.xlabel('Number of Clusters')
plt.ylabel('Sum of Squared Distances')
plt.title('Within Cluster Sum of Squares (WCSS) for Optimal number of clusters\n'
          'K-means with PCA clustering')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.show()

##############################################################
# Once number of clusters are determined, we compute k-means using the reduced PCA output
kmeans = KMeans(init='k-means++', n_clusters=5, n_init=10)
# Extracting clusters and centroids
clusters = kmeans.fit_predict(reduced_data)
centroids = kmeans.cluster_centers_

##############################################################
# PLOTTING: (1) Visualising Clusters over Principal Components
# Transposing the array
reduced_data = reduced_data.T
# Converting it into a df and adding columns with clusters and their labels
df_pca_clusters = pd.DataFrame(reduced_data.T)
df_pca_clusters['Clusters_ns'] = clusters
df_pca_clusters['Clusters'] = df_pca_clusters['Clusters_ns'].map({
    0: 'First',
    1: 'Second',
    2: 'Third'
})

# Plots the clustering applied over PC1 and PC2
fig_B = plt.figure('D5_X_scatter')

ax = sns.scatterplot(df_pca_clusters[0], df_pca_clusters[1],
                     hue=df_pca_clusters['Clusters'], palette = ['#FFFF00', '#FFD700', '#F0E68C'], s=90, alpha=0.75)
plt.scatter(centroids[:, 0], centroids[:, 1],
            marker='x', s=150, linewidths=3,
            color='black', zorder=10, label = 'Centroids')
plt.title('Visualising clusters by principal components')
plt.legend()
plt.xlabel('Principal Component 1 (PC1)')
plt.ylabel('Principal Component 2 (PC2)')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.show()

##############################################################
# (2) Averaged cluster activity for analysing individual sessions (one flavour at a time)


# split full_data into clusters_data
c1_data = full_data[clusters == 0, :] # avg: 1, 360 - 3x (1, 120)
c2_data = full_data[clusters == 1, :] # avg: 1, 360 - 3x (1, 120)
c3_data = full_data[clusters == 2, :] # avg: 1, 360 - 3x (1, 120)


# find average ROIs activity for each cluster. Shape would be 1 x 360
c1_avg = np.mean(c1_data, axis=0)
c2_avg = np.mean(c2_data, axis=0)
c3_avg = np.mean(c3_data, axis=0)


##############################################################
# Plot the means and sem of the two clusters
fig_C = plt.figure('D5_X_sem')
plt.plot(xran, c1_avg, color='#FFFF00')
plt.fill_between(xran, c1_avg - stats.sem(c1_data, axis=0),
                 c1_avg + stats.sem(c1_data, axis=0), color='#FFFF00', alpha=0.2,)
plt.plot(xran, c2_avg, color='#FFD700')
plt.fill_between(xran, c2_avg - stats.sem(c2_data, axis=0),
                 c2_avg + stats.sem(c2_data, axis=0), color='#FFD700', alpha=0.2,)
plt.plot(xran, c3_avg, color='#F0E68C')
plt.fill_between(xran, c3_avg - stats.sem(c3_data, axis=0),
                 c3_avg + stats.sem(c3_data, axis=0), color='#F0E68C', alpha=0.2,)
plt.axvline(0, color='silver', linestyle='--')
plt.title('Means +- SEM of clustered (k=3) ROIs projected over time')
plt.legend(['First Cluster','Second Cluster','Third Cluster','Stimulus Onset'], loc=2)
plt.ylabel('Normalised dF/F')
plt.xlabel('Time(s)')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.show()

##############################################################
