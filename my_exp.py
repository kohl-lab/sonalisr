# Import local tools

# sys.path.append(os.path.expanduser('/Users/anabottura/github/pynalysis/'))

# Setup analysis variables

mouse_id = "CTBD7.1d"
plot = "True"

# Setup path variables

# Sonali's file paths:
output = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/analysed_data"
info_file = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/exp_info.csv"
filepath = "/Users/sonalisriranga/PycharmProjects/RSC/data/pipeline_output/imaging"
rois_file = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/2p_data/rois_analysis" \
            "/%s/rois_mapping/consitent_rois_plane1.csv" % mouse_id
save_path = "/Users/sonalisriranga/PycharmProjects/Pycharm_resources/RSC/analysed_data/%s/" % mouse_id

# Ana's file paths:
# output = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - RSCASSOC/Experiments/2p/analysed_data/"
# info_file = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - RSCASSOC/Experiments/2p/data/exp_info.csv"
# filepath = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - " \
#            "RSCASSOC/Experiments/2p/data/pipeline_output/imaging/ "
# rois_file = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - " \
#             "RSCASSOC/Experiments/2p/rois_analysis/%s/rois_mapping/consitent_rois_plane1.csv" % mouse_id
# save_path = "/Users/anacarolinabotturabarros/University of Glasgow/kohl-lab - " \
#             "RSCASSOC/Experiments/2p/analysed_data/%s/" % mouse_id
