import numpy as np
from sklearn.decomposition import PCA


def roi_traces_by_trial_criteria(sess):
    '''
    Takes trials selected from session object, 2p frames, roi traces
    and returns an array of 2p trace data around specific 2p frame numbers.

    Required Arguments:
          - sessY                     : session object

    Returns:
          - xran (1D array)         : time values for the 2p frames
          - roi_data (2 or 3D array): roi trace data, structured as
                                        ROI x sequences (x frames)
    '''

    list_trials = sess.get_trials_by_criteria(good_trials=120)
    trial_2pfr = sess.get_twop_fr_by_trial(list_trials, ch_fl=[2, 2])
    xran, roi_data = sess.get_roi_trace_array(trial_2pfr, 2, 2, baseline=4)
    return xran, roi_data


###########################################################

def run_pca(Tavg_data, n_comp):
    '''
    Takes the trial averaged matric
    Runs pca and with specific number of components and
    returns pca

    Required Arguments:
    Tavg_data (neuron x time) :  Trial averaged matrix
    n_comp (int)              :  Number of components to run PCA

    Returns:
    pca ( neurons x n_comps)
    '''
    pcamodel = PCA(n_comp)
    pca = pcamodel.fit_transform(Tavg_data)
    return pca

###########################################################

def responsive_traces(sess):
    '''
    Takes trials selected from session object, 2p frames, roi traces
    and returns an array of 2p trace data around specific 2p frame numbers
    and 2p trace data for only responsive rois.

    Required Arguments:
          - sessY                     : session object

    Returns:
          - xran_full (1D array)      : time values for the 2p frames
          - roi_data (2 or 3D array)  : responsive roi traces, structured as
                                        ROI x trials x frames (time)
          - full_data (2 or 3D array): roi traces, structured as
                                     ROI x trials x frames (time)
    '''

    list_trials = sess.get_trials_by_criteria(good_trials=120)
    trial_2pfr = sess.get_twop_fr_by_trial(list_trials, ch_fl=[2, 2])
    # To get the index of rois that were responsive after stimulus onset
    xran, roi_data = sess.get_roi_trace_array(trial_2pfr, 2, 2, baseline=4, resp_rois=True)
    xran_full, full_data = sess.get_roi_trace_array(trial_2pfr, 2, 2,
                                                    baseline=4)
    # Normalise by max value
    max = np.max(full_data)
    full_data = full_data / max

    return roi_data, xran_full, full_data

###########################################################

def tracked_roi_traces(sess):
    '''
    Takes trials selected from session object, 2p frames, roi traces
    and returns an array of 2p trace data around specific 2p frame numbers
    and 2p trace data for only tracked rois.

    Required Arguments:
          - sess                     : session object

    Returns:
          - xran_full (1D array)      : time values for the 2p frames
          - roi_data (2 or 3D array)  : tracked roi traces, structured as
                                        ROI x trials x frames (time)
    '''

    list_trials = sess.get_trials_by_criteria(good_trials=120)
    trial_2pfr = sess.get_twop_fr_by_trial(list_trials, ch_fl=[2, 2])
    # To get the index of rois that were tracked across multiple sessions
    xran, roi_data = sess.get_roi_trace_array(trial_2pfr, 2, 2, baseline=4, tracked_rois=True)

    return xran, roi_data

###########################################################
def get_percentage_resp_roi(sess):
    '''Calculates the percentage of ROIs that are less active, more active and
     unresponsive to the stimulus:
    Required args:
            - sess           : Session Object
    Output:
            - less active    : Proportion of suppressed ROIs
            - more active    : Proportion of excited ROIs
            - unresponsive   : Proportion of unresponsive ROIs
            '''
    roi_data, xran_full, full_data = responsive_traces(sess)

    resp_roi_ns = np.where(sess.resp_rois)[0]
    signif_sign = sess.resp_roi_sign[resp_roi_ns]
    signif_sign_str = ['-' if sign == -1 else '+' for sign in signif_sign]
    roi_strs = [f'{sign}{roi}' for roi, sign in zip(resp_roi_ns, signif_sign_str)]

    roi_int = []
    for value in roi_strs:
        try:
            roi_int.append(int(value))
        except ValueError:
            roi_int.append((value))

    more_active = [val for val in roi_int if val >= 0]
    less_active = [val for val in roi_int if val <= 0]
    less_active[:] = [-1 * x for x in less_active]
    more_act_rois = full_data[more_active, :, :]
    less_act_rois = full_data[less_active, :, :]
    unresponsive = np.delete(full_data,resp_roi_ns, axis=0)

    perc_more_active = np.round(len(more_act_rois) / sess.nrois * 100 , decimals= 2)
    perc_less_active = np.round(len(less_act_rois) / sess.nrois * 100, decimals=2)
    perc_unresponsive = np.round(len(unresponsive) / sess.nrois * 100 , decimals=2)

    return perc_more_active , perc_less_active , perc_unresponsive


###########################################################
def get_Tavg_more_less(sess):
    '''Gathers the trial averaged traces for ROIs that were
       suppressed responsive, excited responsive and
       unresonsive.

    Required args:
            - sess           : session object
    Output:
            - Tavg_less_act : Trial averaged suppressed ROIs
            - Tavg_more_act : Trial averaged excited ROIs
            - Tavg_removed_data : Trial averaged unresponsive ROIs
            '''
    roi_data, xran_full, full_data = responsive_traces(sess)

    resp_roi_ns = np.where(sess.resp_rois)[0]
    signif_sign = sess.resp_roi_sign[resp_roi_ns]
    signif_sign_str = ['-' if sign == -1 else '+' for sign in signif_sign]
    roi_strs = [f'{sign}{roi}' for roi, sign in zip(resp_roi_ns, signif_sign_str)]

    roi_int = []
    for value in roi_strs:
        try:
            roi_int.append(int(value))
        except ValueError:
            roi_int.append((value))

    more_active = [val for val in roi_int if val >= 0]
    less_active = [val for val in roi_int if val <= 0]
    less_active[:] = [-1 * x for x in less_active]
    more_act_rois = full_data[more_active, :, :]
    less_act_rois = full_data[less_active, :, :]
    unresponsive = np.delete(full_data,resp_roi_ns, axis=0)

    Tavg_more_act = np.mean(more_act_rois, axis=1)
    Tavg_less_act = np.mean(less_act_rois, axis=1)
    Tavg_full_data = np.mean(full_data, axis=1)
    Tavg_removed_data = np.delete(Tavg_full_data, resp_roi_ns, axis=0)

    return xran_full, Tavg_less_act , Tavg_more_act, Tavg_removed_data