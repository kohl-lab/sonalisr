"""
session.py

Class to store, extract, and analyze a session recording for
data from the RSCASSOC project (Kohl lab, Dr. Ana Carolina Barros)..

Authors: Colleen Gillon, Ana Carolina Barros

Date: June, 2020

Note: this code uses python 3.7.

"""

import os

import h5py
import numpy as np
import pandas as pd
import scipy.stats as st
# from sklearn.decomposition import PCA, NMF
from oasis.functions import deconvolve

from assoc_utils import assoc_mat_util, assoc_gen_util
from util import file_util, gen_util, math_util


# from oasis.functions import deconvolve


#############################################
#############################################
class Session(object):
    """
    The Session object is the top-level object for analyzing a session
    recording for one area for the Kohl lab RSCASSOC Project. All that needs
    to be provided to create the object is the data directory, mouse ID or
    number, the session date or number and the area number. The Session object
    that is created will contain all of the information relevant to a
    session, including stimulus information, behaviour information and
    pointers to the 2p data.
    """

    def __init__(self, datadir, area, mouseid=None, date=None, mouse_n=None,
                 sess_n=None, mouse_df='mouse_df.csv', roi_file="consitent_rois_plane1_b15.csv"):
        """
        self.__init__(datadir, area)

        Initializes and returns the new Session object using the specified
        data directory, mouse ID or number, session date or number and
        area number.

        Sets attributes:
            - datapath (str): path to the data file (.mat)
            - home (str)    : path of the master data directory

        and calls self._init_attribs() and
                  self._get_mat_data()

        Required args:
            - datadir (str): full path to the directory where data is stored
            - area (int)   : area

        Optional args:
            - mouseid (str)        : mouse ID (must provide mouse ID or number,
                                     but not both)
                                     default: None
            - date (str or int)    : date, in YYYYMMDD format (must provide date
                                     or session number, but not both)
                                     default: None
            - mouse_n (str or int) : mouse number (must provide mouse ID or
                                     number, but not both)
                                     default: None
            - sess_n (str or int)  : session number (must provide date or
                                     session number, but not both)
                                     default: None
            - mouse_df (str)       : path name of dataframe containing
                                     information on each recorded session.
                                     Dataframe should have the following
                                     columns:
                                         exp_n,task,mouse_n,mouseid,date,area,
                                         flavour_code,flavour_id,sess_n,ITI,depth,
                                         n_frames,fr,zoom,n_trials
                                     default: 'mouse_df.csv'
            - plane_oi (str or int): plane of interest
                                     default: 'both'
        """
        # looks into the datadir
        self.home = datadir
        self.roi_file = roi_file
        # sets attributes
        self._init_attribs(area, mouseid, date, mouse_n, sess_n, mouse_df)

        self.datapath = os.path.join(self.home, '{}.mat'.format(self.mouseid))
        # gets trial data from the .mat files
        self._get_mat_data()

    #############################################
    def __repr__(self):
        return (f'{self.__class__.__name__} '
                f'(M{self.mouse_n}, S{self.sess_n}, A{self.area})')

    def __str__(self):
        return repr(self)

    #############################################
    def _init_attribs(self, area, mouseid=None, date=None, mouse_n=None,
                      sess_n=None, mouse_df='mouse_df.csv'):
        """
        self._init_attribs(area)

        Sets the attributes for a specific session and area based on the mouse
        dataframe:

        Sets attributes:
            - exp_n (int)       : experiment number
            - area (int)        : area
            - date (int)        : session date (in YYYYMMDD format)
            - mouseid (str)     : mouse ID (e.g., CTBD7.2e)
            - mouse_n (int)     : mouse number (corresponds to mouse ID)
            - flavour_id (str)  : flavour id (e.g., Water)
            - flavour_code (str): flavour code (corresponds to flavour ID)
            - sess_n (int)      : session number (corresponds to session date)
            - task (str)        : session task
            - ITI (int)         : inter-trial-interval
            - n_trials (int)    : number of trials per flavour
                                  (-1 if no 2p recording or no ROIs extracted)

        Required args:
            - area (int)   : area

        Optional args:
            - mouseid (str)       : mouse ID (must provide mouse ID or number,
                                    but not both)
                                    default: None
            - date (str or int)   : date, in YYYYMMDD format (must provide date
                                    or session number, but not both)
                                    default: None
            - mouse_n (str or int): mouse number (must provide mouse ID or
                                    number, but not both)
                                    default: None
            - sess_n (str or int) : session number (must provide date or
                                    session number, but not both and should match the area)
                                    default: None
            - mouse_df (str)      : path name of dataframe containing
                                    information on each recorded session.
                                    Dataframe should have the following
                                    columns:
                                        mouse_n, mouseid, sess_n, date, task,
                                        area, n_trials
                                    default: 'mouse_df.csv'
        """
        # If mouse_df is a str load it using the file_util.loadfile and save in variable mouse_df
        if isinstance(mouse_df, str):
            mouse_df = file_util.loadfile(mouse_df)

        # collect mouse and session number information to make lists
        info = [[mouseid, mouse_n], [date, sess_n]]
        labs = [['mouseid', 'mouse_n'], ['date', 'sess_n']]

        # This chunk tells it to accept mouseid or mouse_n / date or sess_n but not both
        for s in range(len(info)):
            done = False
            if len(info[s]) != 2:
                raise ValueError(
                    'Code must be modified if the sublists are not of '
                    'length 2.')
            for i in range(len(info[s])):
                if info[s][i] is None:
                    if info[s][1 - i] is None:
                        raise ValueError(
                            'Must provide either `{}` or `{}`.'.format(
                                labs[s][i], labs[s][1 - i]))
                    elif done:
                        raise ValueError(
                            'Cannot provide both `{}` and `{}`.'.format(
                                labs[s][i], labs[s][1 - i]))
                    else:
                        if s == 0:
                            info[s][i] = gen_util.get_df_vals(
                                mouse_df, [labs[s][1 - i], 'area'], [info[s][1 - i], area],
                                labs[s][i], single=True)
                        else:
                            info[s][i] = gen_util.get_df_vals(
                                mouse_df, [labs[s][1 - i], 'mouseid', 'area'],
                                [info[s][1 - i], info[0][0], area],
                                labs[s][i], single=True)
                        done = True

        self.mouseid, self.mouse_n = info[0]
        self.date, self.sess_n = info[1]

        self.area = area
        areas = gen_util.get_df_vals(
            mouse_df, ['mouseid', 'date'], [self.mouseid, self.date], 'area',
            dtype=str)

        self.plane = 1

        if str(self.area) not in areas:
            raise ValueError(
                'No area {} found for mouse {}, {} session.'.format(
                    self.area, self.mouseid, self.date))

        df_line = gen_util.get_df_vals(
            mouse_df, ['mouseid', 'date', 'sess_n'],
            [self.mouseid, self.date, self.sess_n], single=True)

        # Added ITI, flavour_code, flavour_id to df_line
        self.task = df_line['task'].tolist()[0]
        self.n_trials = int(df_line['n_trials'].tolist()[0])
        self.flavour_code = df_line['flavour_code'].tolist()[0]
        self.flavour_id = df_line['flavour_id'].tolist()[0]
        self.fr = df_line['fr'].tolist()[0]

        #############################################

    def _make_trial_df(self):
        """
            self._make_trial_df()

            Saves a dataframe containing trial information as an attribute.

            Sets attributes:
                - trial_df (pd DataFrame): trial alignment dataframe with columns:
                                           'trial_n', 'stim_id', 'outcome, 'rew_2pfr',
                                           'first_lick2pfr'
        """

        # create a dataframe with column names as below:
        self.trial_df = pd.DataFrame(
            columns=['trial_n', 'stim_id', 'rew_2pfr', 'first_lick2pfr'])

        # find reward frames and add to trial_df:
        # sets the length of trial_start to the length
        trial_start = self.trial_start[:len(self.iti_start)]
        self.trial_df['rew_2pfr'] = trial_start.tolist()

        # find first licks and add it to trial_df
        first_lick = []
        for t, trial in enumerate(self.trial_df['rew_2pfr']):
            array = self.licks[self.licks >= trial]
            if t+1 == len(self.trial_df['rew_2pfr']):
                array = array
            else:
                array = array[array < self.trial_df['rew_2pfr'][t+1]]
            if not array.any():
                array = [None]
            first_lick.append(array[0])
        # print(len(first_lick))
        # print(len(self.trial_df['first_lick2pfr']))
        self.trial_df['first_lick2pfr'] = first_lick

        self.trial_df = self.trial_df[self.trial_df['first_lick2pfr'].notna()]

        self.trial_df['first_lick2pfr'] = self.trial_df['first_lick2pfr'].astype(int)

        # Add trial_n as the length of iti_start
        self.trial_df['trial_n'] = list(range(len(self.trial_df['first_lick2pfr'])))
        self.trial_df['trial_n'] = self.trial_df['trial_n']+1

        # the stim_id is the flavour id
        self.trial_df['stim_id'] = self.flavour_id

        self.trial_df.reset_index(drop=True, inplace=True)

    #############################################
    def _get_mat_data(self):
        """
            self._get_mat_data()

            Sets the attributes for a specific session area based on the mouse data
            file.

            Also calls self._make_df()

            Sets attributes:
                - licks (1D array)         : 2p frames at which each lick started
                - trial_start (1D array)   : 2p frames at which reward was delivered
                - lick_window (1D array)   : 2p frame lick window. Trials considered as valid if
                                             they occur during this window
                - n_trials (int)           : number of trials
                - iti_start (1D array)     : 2p frames of inter-trial-intervals
                - run (1D array)           : run speed for each 2p frame
                -

            """

        date = assoc_mat_util.make_date_key(self.date)
        area = assoc_mat_util.make_area_key(self.area)
        plane = assoc_mat_util.make_plane_key(1)

        with h5py.File(self.datapath, 'r') as f:
            self.run = assoc_mat_util.get_run_speed(f, date, area)
            self.trial_start = assoc_mat_util.get_trial_start(f, date, area)
            self.lick_window = assoc_mat_util.get_lick_window(f, date, area)
            self.iti_start = assoc_mat_util.get_iti_start(f, date, area)
            self.licks = assoc_mat_util.get_licks(f, date, area)
            self.fps = assoc_mat_util.get_fps(f, date, area)
            self.ttl = assoc_mat_util.get_tlls(f, date, area)
            self.twop_fps = assoc_mat_util.get_fps(f, date, area, plane)
            self.roipos = assoc_mat_util.get_roipos(
                f, date, area, plane) #todo adapt this to nan values
            fluo_corr = assoc_mat_util.get_fluo_corr(
                f, date, area, plane).T
            self.fluo_corr = self._check_traces(fluo_corr)
            fluo_raw = assoc_mat_util.get_fluo_raw(
                f, date, area, plane).T
            self.fluo_raw = self._check_traces(fluo_raw, self.nan_array)
            neuropil_raw = assoc_mat_util.get_neuropil_raw(
                f, date, area, plane).T
            self.neuropil_raw = self._check_traces(neuropil_raw, self.nan_array)
            self.fluo_trbytr = assoc_mat_util.get_fluo_trialbytrial(
                f, date, area, plane)
            self.tot_twop_fr = self.fluo_corr.shape[1]
            self.nrois = self.fluo_corr.shape[0]

        self._make_trial_df()

    ############################################
    def _set_run_trials(self, w_before=60, w_after=60, threshold=40):

        running_trials = []
        for t, trial in enumerate(self.trial_df['first_lick2pfr']):
            if np.isnan(trial):
                running_trials.append(None)
            else:
                fr_before = trial-w_before
                if fr_before < 0:
                    fr_before = 0
                fr_after = trial+w_after
                if fr_after > self.ttl[1]:
                    fr_after = self.ttl[1]
                speed_window = self.run[int(fr_before):int(fr_after)]
                running = np.mean(speed_window) > threshold
                running_trials.append(running)

        # self.trial_df = pd.concat([self.trial_df, pd.Series(running_trials, name='running_trials')], axis=1)
        return running_trials


    def _set_good_trials(self, window=120):

        diff = self.trial_df['first_lick2pfr'] - self.trial_df['rew_2pfr']
        good_trials = pd.Series(diff < window)
        #
        # self.trial_df = self.trial_df.dropna(axis=0, how='any', thresh=3)
        return good_trials

    ############################################
    def _format_trial_criteria(self, trial_n='any', stim_id='any',
                                rew_2pfr='any', first_lick2pfr='any',
                               good_trials='any', running='any'):
        """
        self._format_trial_criteria()

        Returns a list of trial parameters formatted correctly to use
        as criteria when searching through the trial dataframe.

        Will strip criteria not related to the current session object.

        Optional args:
            - trial_n (str, int or list)    : trial number value(s) of interest
                                                default: 'any'
            - stim_id (str, int or list)    : stim_id value(s) of interest
                                                default: 'any'
            - rew_2pfr (str or list)        : Reward delivery 2p frames range of interest
                                                [min, max (excl)]
                                                default: 'any'
            - first_lick2pfr (str or list)  : 2p end frames range of interest
                                                [min, max (excl)]
                                                default: 'any'
            - good_trials (str or int)    : number of frames for window
                                                default: 'any'
            - running (str or list)        : list with [threshold, before, after]
                                                default: 'any'

        Returns:
            - trial_n (list)     : trial number value(s) of interest
            - stim_pos (list)    : stim_pos value(s) of interest
            - outcome (list)     : outcome value(s) of interest,
            - start2pfr_min (int): minimum of 2p start2pfr range of interest
            - start2pfr_max (int): maximum of 2p start2pfr range of interest
                                   (excl)
            - end2pfr_min (int)  : minimum of 2p end2pfr range of interest
            - end2pfr_max (int)  : maximum of 2p end2pfr range of interest
                                   (excl)
            - num2pfr_min (int)  : minimum of num2pfr range of interest
            - num2pfr_max (int)  : maximum of num2pfr range of interest
                                   (excl)
        """

        # converts values to lists or gets all possible values, if 'any'
        stim_id = gen_util.get_df_label_vals(
            self.trial_df, 'stim_id', stim_id)
        trial_n = gen_util.get_df_label_vals(
            self.trial_df, 'trial_n', trial_n)

        if rew_2pfr in ['any', None]:
            rew_2pfr_min = int(self.trial_df['rew_2pfr'].min())
            rew_2pfr_max = int(self.trial_df['rew_2pfr'].max() + 1)
        else:
            rew_2pfr_min = int(rew_2pfr[0])
            rew_2pfr_max = int(rew_2pfr[1])
        if first_lick2pfr in ['any', None]:
            first_lick2pfr_min = int(self.trial_df['first_lick2pfr'].min())
            first_lick2pfr_max = int(self.trial_df['first_lick2pfr'].max() + 1)
        else:
            first_lick2pfr_min = int(first_lick2pfr[0])
            first_lick2pfr_max = int(first_lick2pfr[1])
        if good_trials in ['any', None]:
            good_trials = np.ones(len(self.trial_df['trial_n']), dtype = bool).tolist()
        else:
            good_trials = self._set_good_trials(good_trials)

        if running in ['any', None]:
            running = np.ones(len(self.trial_df['trial_n']), dtype = bool).tolist()
        else:
            running = self._set_run_trials(running[1], running[2], running[0])

        return [trial_n, stim_id, rew_2pfr_min, rew_2pfr_max,
                first_lick2pfr_min, first_lick2pfr_max, good_trials, running]

    #     #############################################
    #
    # def get_plane(self, plane=1):
    #     """
    #     self.get_plane()
    #
    #     Returns the requested plane.
    #
    #     Optional args:
    #         - plane (int or str): plane
    #
    #     Returns:
    #         - ret_pl (Plane): requested plane
    #     """
    #
    #     if int(plane) == 1:
    #         ret_pl = self.plane_1
    #     else:
    #         raise ValueError('No plane {}.'.format(plane))
    #
    #     return ret_pl
    #
    # ############################################
    def get_trials_by_criteria(self, trial_n='any', stim_id='any',
                               rew_2pfr='any', first_lick2pfr='any',
                               good_trials='any', running='any'):
        """
        self.get_trials_by_criteria()

        Returns a list of trial numbers that have the specified values
        in specified columns in the trial dataframe.

        Optional args:
            - trial_n (str, int or list)   : trial number value(s) of interest
                                             default: 'any'
            - stim_id (str, int or list)   : stim_id value(s) of interest
                                             default: 'any'
            - rew_2pfr (str or list)       : 2p start frames range of interest
                                             [min, max (excl)]
                                             default: 'any'
            - first_lick2pfr (str or list) : 2p end frames range of interest
                                             [min, max (excl)]
                                             default: 'any'
            - good_trials (bool)           : if True, gets trials when lick was inside window
                                             default: 'any'
            - running_trials (bool):       : if True, gets trials when animal is running
                                             default: 'any'
            - remconsec (bool)             : if True, consecutive segments are
                                             removed within a block
                                             default: False

        Returns:
            - trial_ns (list): list of trial numbers that obey the criteria
        """

        pars = self._format_trial_criteria(
            trial_n, stim_id, rew_2pfr, first_lick2pfr, good_trials, running)

        [trial_n, stim_id, rew_2pfr_min, rew_2pfr_max,
         first_lick2pfr_min, first_lick2pfr_max, good_trials, running] = pars

        trial_ns = self.trial_df.loc[
            (self.trial_df['trial_n'].isin(trial_n)) &
            (self.trial_df['stim_id'].isin(stim_id)) &
            (self.trial_df['rew_2pfr'] >= rew_2pfr_min) &
            (self.trial_df['rew_2pfr'] < rew_2pfr_max) &
            (self.trial_df['first_lick2pfr'] >= first_lick2pfr_min) &
            (self.trial_df['first_lick2pfr'] < first_lick2pfr_max) &
            (good_trials) &
            (running)]['trial_n'].tolist()

        # # if removing consecutive values
        # if remconsec:
        #     trial_ns_new = []
        #     for k, val in enumerate(trial_ns):
        #         if k == 0 or val != trial_ns[k - 1] + 1:
        #             trial_ns_new.extend([val])
        #     trial_ns = trial_ns_new
        # check for empty
        if len(trial_ns) == 0:
            raise ValueError('No trials fit these criteria.')

        return trial_ns

        ##############################################
    def set_resp_rois(self, pre=1, post=1, p_thr=0.01, good_trials=120, replace=False):
        # todo
        """
        self.set_resp_rois()

        Identifies ROIs that are responsive to the stimulus.

        Sets attribute:
            - resp_rois (2D array)    : bool array indicating which ROIs are
                                        responsive to stimulus
            - resp_roi_sign (2D array): array indicating sign (-1/1) of change
                                        in activity locked to stimulus onset
                                        for each ROI, (0 if no trials),

        Optional args:
            - pre (num)     : number of seconds to take before trial onset
                              (None will set to default)
                              default: 1
            - post (num)    : number of seconds to take after trial onset
                              (None will set to default)
                              default: 1
            - p_thr (float) : p value threshold for paired t-test (multiple
                              comparisons not done, 2-tailed)
                              default: 0.01
            - replace (bool): if replace is True, existing responsive ROIs are
                              replaced (e.g., if using new parameters)
                              default: False
        """
        # setting defaults
        if pre is None:
            pre = 1
        if post is None:
            post = 1

        ch_fl = [pre, post]

        if hasattr(self, 'resp_rois'):
            if replace:
                print(('    Replacing responsive ROI attribute for '
                    'plane {}.').format(self.plane))
            else:
                return

        self.denoise_plane()

        self.resp_rois = np.zeros(self.nrois, dtype=bool)
        self.resp_roi_sign = np.zeros(self.nrois, dtype=int)

        trials = self.get_trials_by_criteria(good_trials=good_trials)
        twop_ref_fr = self.get_twop_fr_by_trial(trials, ch_fl=ch_fl)
        if len(twop_ref_fr) == 0:
            pass
        # ROI x trials
        pre_data = self.get_roi_trace_array(
            twop_ref_fr, pre, 0, integ=True,
            trace_type='denoised')[1]/float(pre)
        post_data = self.get_roi_trace_array(
            twop_ref_fr, 0, post, integ=True,
            trace_type='denoised')[1]/float(post)
        for r in range(len(pre_data)): # 2-tailed
            self.resp_roi_sign[r] = np.sign(np.mean(
                post_data[r] - pre_data[r]))
            if len(trials) > 1:
                _, p_val = st.ttest_rel(pre_data[r], post_data[r])
                self.resp_rois[r] = p_val < p_thr


    ##############################################
    def get_roi_trace_array(self, twop_ref_fr, pre, post, trace_type='corr',
                            integ=False, baseline=None, stats='mean',
                            resp_rois=False, tracked_rois=False):
        """
        self.get_roi_trace_array(twop_ref_fr, pre, post)

        Returns an array of 2p trace data around specific 2p frame numbers.

        Required args:
            - twop_ref_fr (list): 1D list of 2p frame numbers
                                  (e.g., all 1st Gabor A frames)
            - pre (num)         : range of frames to include before each
                                  reference frame number (in s)
            - post (num)        : range of frames to include after each
                                  reference frame number (in s)

        Optional args:
            - trace_type (str) : type of traces
                                 ('corr' for corrected fluorescence,
                                  'raw' for raw fluorescence,
                                  'denoised' for denoised corrected
                                  fluorescence)
                                 default: 'corr'
            - integ (bool)     : if True, dF/F is integrated over frames
                                 default: False
            - baseline (num)   : number of seconds from beginning of
                                 sequences to use as baseline. If None, data
                                 is not baselined.
                                default: None
            - stats (str)      : statistic to use for baseline, mean ('mean')
                                 or median ('median')
                                 default: 'mean'
            - resp_rois (bool) : if True, only responsive ROIs are retained
                                 (responsive to any stim position)
                                 default: False

        Returns:
            - xran (1D array)           : time values for the 2p frames
            - data_array (2 or 3D array): roi trace data, structured as
                                          ROI x sequences (x frames)
        """

        self._check_rois()

        fr_idx, xran = self.get_twop_fr_ran(twop_ref_fr, pre, post)

        # get dF/F: ROI x seq x fr
        data_array = self.get_roi_seqs(fr_idx, trace_type=trace_type)

        if resp_rois:
            self.set_resp_rois()
            data_array = data_array[self.resp_rois]

        if baseline is not None:  # calculate baseline and subtract
            if baseline > pre + post:
                raise ValueError('Baseline greater than sequence length.')
            baseline_fr = int(np.around(baseline * self.twop_fps))
            baseline_data = data_array[:, :, : baseline_fr]
            data_array_base = math_util.mean_med(
                baseline_data, stats=stats, axis=-1)[:, :, np.newaxis]
            data_array = data_array - data_array_base

        if integ:
            data_array = math_util.integ(data_array, 1. / self.twop_fps, axis=2)

        if tracked_rois:
           self._get_tracked_neurons()
           # todo: create function to update self.tracked neurons
           data_array = data_array[self.tracked_rois, :, :]

        data_array = self._check_traces(data_array)

        return xran, data_array

    #############################################

    def _check_rois(self):
        """
        self._check_rois()

        Throws an error if the plane has no ROIs.
        """

        if self.nrois is None:
            raise ValueError('Plane does not have ROIs.')



    #############################################

    def _get_tracked_neurons(self):
        """
        self._get_tracked_neurons


        """

        if isinstance(self.roi_file, str):
             roi_df = file_util.loadfile(self.roi_file)
             roi_df = roi_df - 1

             ind_list = self.nan_array[:, 0].tolist()
             indices = [i + 1 for i, k in enumerate(ind_list) if k == False]
             if indices:
                 for k, neuron in enumerate(roi_df[str(self.sess_n)]):
                     if neuron > indices[0]:
                         roi_df.loc[k, str(self.sess_n)] = neuron - 1
             roi_list = roi_df[str(self.sess_n)]
             self.tracked_rois = roi_list
        else:
            raise ValueError('No file found')

    ##############################################

    def denoise_plane(self, ginit='single_fit'):
        """
        self.denoise_plane()

        Denoise the corrected fluorescence traces for the plane.

        Sets attributes:
            - baselines (1D array): baseline values for each ROI
            - gvalues (list)      : autoregressive model estimated parameters
                                    for each ROI
            - lambdas (1D array)  : optimal La Grange multipliers for each ROI
            - spikes (2D array)   : array of discretized spikes for each frame
                                    (not binary), structured as ROI x frame
            - tau_d (float)       : decay time constant (None if none used to
                                    estimate autocovariance)
            - tau_r (float)       : rising time constant (None if none used to
                                    estimate autocovariance)
            - traces (2D array)   : denoised fluorescence traces, structured as
                                    ROI x frame
        """

        self._check_rois()

        if ginit == 'double_fix':
            self.tau_r = 45e-3
            self.tau_d = 140e-3
            g1 = np.exp(-1 / (self.tau_d * self.twop_fps)) + \
                 np.exp(-1 / (self.tau_r * self.twop_fps))
            g2 = -np.exp(-1 / (self.tau_d * self.twop_fps)) * \
                 np.exp(-1 / (self.tau_r * self.twop_fps))
        else:
            self.tau_r = None
            self.tau_d = None
            g1, g2 = None, None

        ginits = {'double_fix': (g1, g2),
                  'double_fit': (None, None),
                  'single_fix': (0.95,),
                  'single_fit': (None,)
                  }

        if not hasattr(self, 'ginit'):
            self.ginit = None
        else:
            if self.ginit != ginit:
                print(('    Replacing {} with {} denoised spikes for '
                       'plane {}').format(self.ginit, ginit, self.plane))
            else:
                return

        self.ginit = ginit
        if self.ginit not in ginits.keys():
            gen_util.accepted_values_error('ginit', ginit, ginits.keys())

        denoised = np.empty_like(self.fluo_corr)  # ROI x frames
        spikes = np.empty_like(self.fluo_corr)
        baselines = np.empty(self.nrois)
        gvalues = []  # may be tuples for each ROI
        lambdas = np.empty(self.nrois)
        for roi_n in range(self.nrois):  # each ROI
            c, s, b, g, l = deconvolve(
                self.fluo_corr[roi_n, :].astype(np.double),
                g=ginits[self.ginit], penalty=1)
            denoised[roi_n, :] = c
            spikes[roi_n, :] = s
            baselines[roi_n] = b
            gvalues.append(g)
            lambdas[roi_n] = l

        self.denoised = denoised
        self.spikes = spikes
        self.baselines = baselines
        self.gvalues = gvalues
        self.lambdas = lambdas

    #############################################

    def get_twop_fr_by_trial(self, triallist, ch_fl=None):
        """
        self.get_twop_fr_by_seg(triallist)

        Returns a list of arrays containing the 2-photon frame numbers that
        correspond to a given set of trial numbers provided in a list
        for a specific stimulus.

        Required args:
            - triallist (list of ints): the trial number for which to get
                                        2p frames

        Optional args:
            - first (bool): instead, return first frame for each trial
                            default: False
            - last (bool) : instead return last frame for each trial
                            default: False
            - ch_fl (list): if provided, flanks in sec [pre sec, post sec]
                            around frames to check for removal if out of bounds
                            default: None

        Returns:
            if first and last are True:
                - frames (nested list): list of the first and last 2p frames
                                        numbers for each segment, structured
                                        as [first, last]
            if first or last is True, but not both:
                - frames (list)       : a list of first or last 2p frames
                                        numbers for each segment
            else:
                - frames (list of int arrays): a list (one entry per segment)
                                               of arrays containing the 2p
                                               frame
        """

        # initialize the frames list
        frames = []

        # get the rows in the alignment dataframe that correspond to the segments
        rows = self.trial_df.loc[
            (self.trial_df['trial_n'].isin(triallist))]

        # get the start frames and end frames from each row
        start2pfrs = rows['first_lick2pfr'].values

        if ch_fl is not None:
            start2pfrs = self.check_flanks(start2pfrs, ch_fl)

        frames = start2pfrs
        frames = gen_util.delist_if_not(frames)

        return frames

    #############################################################

    def check_flanks(self, frs, ch_fl, fr_type='twop'):
        """
        self.check_flanks(self, frs, ch_fl)

        Required args:
            - frs (arraylike): list of frames values
            - ch_fl (list)   : flanks in sec [pre sec, post sec] around frames
                               to check for removal if out of bounds

        Optional args:
            - fr_type (str): time of frames ('twop', 'stim')
                             default: 'twop'

        """

        if not isinstance(ch_fl, list) or len(ch_fl) != 2:
            raise ValueError('`ch_fl` must be a list of length 2.')

        fps = self.twop_fps
        max_val = self.tot_twop_fr

        ran_fr = [np.around(x * fps) for x in [-ch_fl[0], ch_fl[1]]]
        frs = np.asarray(frs)

        neg_idx = np.where((frs + ran_fr[0]) < 0)[0].tolist()
        over_idx = np.where((frs + ran_fr[1]) >= max_val)[0].tolist()
        all_idx = set(range(len(frs))) - set(neg_idx + over_idx)

        if len(all_idx) == 0:
            frs = np.asarray([])
        else:
            frs = frs[np.asarray(sorted(all_idx))]

        return frs

        #############################################

    def get_twop_fr_ran(self, twop_ref_fr, pre, post):
        """
        self.get_twop_fr_ran(twop_ref_fr, pre, post)

        Returns an array of 2p frame numbers, where each row is a sequence and
        each sequence ranges from pre to post around the specified reference
        2p frame numbers.

        Required args:
            - twop_ref_fr (list): 1D list of 2p frame numbers
                                  (e.g., all 1st seg frames)
            - pre (num)         : range of frames to include before each
                                  reference frame number (in s)
            - post (num)        : range of frames to include after each
                                  reference frame number (in s)

        Returns:
            - num_ran (2D array): array of frame numbers, structured as:
                                      sequence x frames
            - xran (1D array)   : time values for the 2p frames

        """

        self._check_rois()

        ran_fr = [np.around(x * self.twop_fps)for x in [-pre, post]]
        xran = np.linspace(-pre, post, int(np.diff(ran_fr)[0]))

        if len(twop_ref_fr) == 0:
            raise ValueError(
                'No frames: frames list must include at least 1 frame.')

        if isinstance(twop_ref_fr[0], (list, np.ndarray)):
            raise ValueError(
                'Frames must be passed as a 1D list, not by block.')

        # get sequences x frames
        fr_idx = gen_util.num_ranges(
            twop_ref_fr, pre=-ran_fr[0], leng=len(xran))

        # remove sequences with negatives or values above total number of stim
        # frames
        neg_idx = np.where(fr_idx[:, 0] < 0)[0].tolist()
        over_idx = np.where(fr_idx[:, -1] >= self.tot_twop_fr)[0].tolist()

        num_ran = gen_util.remove_idx(fr_idx, neg_idx + over_idx, axis=0)

        if len(num_ran) == 0:
            raise ValueError('No frames: All frames were removed from list.')

        return num_ran, xran

    #############################################
    def get_roi_seqs(self, twop_fr_seqs, padding=(0, 0), trace_type='corr',
                     basewin=1000):
        """
        self.get_roi_seqs(stim_fr_seqs)

        Returns the processed ROI traces for the given trials.
        Frames around the start and end of the trials can be requested by
        setting the padding argument.

        If the sequences are different lengths the array is nan padded

        Required args:
            - twop_fr_seqs (list of arrays): list of arrays of 2p frames,
                                             structured as sequences x frames.
                                             If any frames are out of range,
                                             then NaNs returned.


        Optional args:
            - padding (2-tuple of ints): number of additional 2p frames to
                                         include from start and end of
                                         sequences
                                         default: (0, 0)
            - trace_type (str)         : type of traces
                                         ('corr' for corrected fluorescence,
                                          'raw' for raw fluorescence,
                                          'denoised' for denoised corrected
                                           fluorescence)
                                          default: 'corr'
            - basewin (int)            : window length for calculating baseline
                                         fluorescence
                                         default: 1000

        Returns:
            - traces (3D array): array of traces for the specified
                                 ROIs and sequences, structured as:
                                 ROIs x sequences x frames
        """

        self._check_rois()

        # extend values with padding
        if padding[0] != 0:
            min_fr = np.asarray([min(x) for x in twop_fr_seqs])
            st_padd = np.tile(
                np.arange(-padding[0], 0),
                (len(twop_fr_seqs), 1)) + min_fr[:, None]
            twop_fr_seqs = [np.concatenate((st_padd[i], x))
                            for i, x in enumerate(twop_fr_seqs)]
        if padding[1] != 0:
            max_fr = np.asarray([max(x) for x in twop_fr_seqs])
            end_padd = np.tile(
                np.arange(1, padding[1] + 1),
                (len(twop_fr_seqs), 1)) + max_fr[:, None]
            twop_fr_seqs = [np.concatenate((x, end_padd[i]))
                            for i, x in enumerate(twop_fr_seqs)]
        if padding[0] < 0 or padding[1] < 0:
            raise ValueError('Negative padding not supported.')

        # get length of each padded sequence
        pad_seql = np.array([len(s) for s in twop_fr_seqs])

        # flatten the sequences into one list of frames, removing any sequences
        # with unacceptable frame values (< 0 or > self.tot_twop_fr)
        frames_flat = np.empty([sum(pad_seql)])
        last_idx = 0
        seq_rem = []
        seq_rem_l = []
        for i in range(len(twop_fr_seqs)):
            if (max(twop_fr_seqs[i]) >= self.tot_twop_fr or
                    min(twop_fr_seqs[i]) < 0):
                seq_rem.extend([i])
                seq_rem_l.extend([pad_seql[i]])
            else:
                frames_flat[last_idx: last_idx + pad_seql[i]] = twop_fr_seqs[i]
                last_idx += pad_seql[i]

        # Warn about removed sequences and update pad_seql and twop_fr_seqs
        # to remove these sequences
        if len(seq_rem) != 0:
            print(('\nSome of the specified frames for sequences {} are out of '
                   'range so the sequence will not be included.').format(seq_rem))
            pad_seql = np.delete(pad_seql, seq_rem)
            twop_fr_seqs = np.delete(twop_fr_seqs, seq_rem).tolist()

        # sanity check that the list is as long as expected
        if last_idx != len(frames_flat):
            if last_idx != len(frames_flat) - sum(seq_rem_l):
                raise ValueError((
                    'Concatenated frame array is {} long instead of '
                    'expected {}.').format(
                    last_idx, len(frames_flat - sum(seq_rem_l))))
            else:
                frames_flat = frames_flat[: last_idx]

        # convert to int
        frames_flat = frames_flat.astype(int)

        # load the traces
        full_traces = self.get_traces(trace_type)
        traces_flat = full_traces[:, frames_flat]

        # chop back up into sequences padded with Nans
        traces = np.full((self.nrois, len(twop_fr_seqs), max(pad_seql)), np.nan)

        last_idx = 0
        for i in range(len(twop_fr_seqs)):
            traces[:, i, :pad_seql[i]] = \
                traces_flat[:, last_idx:last_idx + pad_seql[i]]
            last_idx += pad_seql[i]

        return traces
    ##################################################

    def get_traces(self, trace_type='corr'):
        """
        self.get_traces()

        Returns the requested traces.

        Optional args:
            - trace_type (str): type of traces
                                ('corr' for corrected fluorescence,
                                 'raw' for raw fluorescence,
                                 'denoised' for denoised corrected
                                 fluorescence)
                                default: 'corr'

        Returns:
            - traces (2D array): traces, structured as ROI x frame
        """

        self._check_rois()

        if trace_type == 'corr':
            traces = self.fluo_corr
        elif trace_type == 'raw':
            traces = self.fluo_raw
        elif trace_type == 'denoised':
            if not hasattr(self, 'denoised'):
                raise ValueError('If `trace_type` is `denoised`, first call '
                    '`self.denoised_plane` on the plane.')
            else:
                traces = self.denoised
        else:
            gen_util.accepted_values_error(
                'trace_type', trace_type, ['corr', 'raw', 'denoised'])

        return traces

    ##################################################

    def _check_traces(self, traces, nan_array=[]):

        if len(nan_array) == 0:
            nans = np.isnan(traces)
            non_nans = ~ nans
            if np.sum(nans) > 0:
                traces = traces[non_nans].reshape(-1, traces.shape[1])

            self.nan_array = non_nans

        else:
            traces = traces[nan_array].reshape(-1, traces.shape[1])

        return traces

    ##################################################

